<?php
	include_once dirname(__FILE__) . '/./ASEngine/AS.php';

	if (! app('login')->isLoggedIn()) {
	    redirect("login.php");
	}

	include_once 'profunctions.php';
	$db = getDbConnection();

	$userid = ASSession::get('user_id');
	if ( !isAgencyUser($userid) ) {
		echo "
	<script>
		window.location.href = '404.html';
	</script>";
		exit;
	}

	if ( $_GET['action'] == 'delete' ) {
		$stmt = $db->prepare('DELETE FROM CustomerVerify WHERE id = :id and created_by = :userid');
        $stmt->execute(array(":id" => $_GET['id'], ":userid" => $userid));
        echo json_encode( array('result' => 'success') );
		exit;
	}

	if ( $_GET['action'] == 'create' ) {
		$stmt = $db->prepare('insert into CustomerVerify values (null, :email, :created_by)');
		$res = $stmt->execute(array(":email" => trim($_POST['email']), ":created_by" => $userid));
		echo json_encode( array('result' => 'success') );
		exit;
	}

	if ( $_GET['action'] == 'update' ) {
		$stmt = $db->prepare('update CustomerVerify set email=:email where id=:id and created_by = :userid');
		$stmt->execute(array(":email" => trim($_POST['email']), ":id" => $_GET['id'], ":userid" => $userid));
		echo json_encode( array('result' => 'success') );
		exit;
	}

	include 'templates/header.php';
	$sidebarActive = 'agency';
	require 'templates/sidebar.php';
?>

<section id="main-content">
	<section class="wrapper">
		<div class="container-fluid">
			<div class="btn-group">
				<button id="editable-sample_new" class="btn btn-primary">
					Add New <i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="editable-sample">
				<thead>
					<tr>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$pdStmt = $db->prepare("SELECT * FROM CustomerVerify WHERE created_by = :userid");
					$pdStmt->execute(array(":userid" => $userid));
					$results = $pdStmt->fetchAll(PDO::FETCH_ASSOC);

					foreach ($results as $result) {
				?>
					<tr data-id="<?= $result["id"] ?>">
						<td><?= $result["email"] ?></td>
						<td><a class="edit" href="javascript:;">Edit</a></td>
						<td><a class="delete" href="javascript:;">Delete</a></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>

	</section>
</section>

<script type="text/javascript">
	var EditableTable = function () {

	return {
		init: function () {
			function restoreRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);

				for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
					oTable.fnUpdate(aData[i], nRow, i, false);
				}

				oTable.fnDraw();
			}

			function editRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);
				jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<a class="edit" href="">Save</a>';
				jqTds[2].innerHTML = '<a class="cancel" href="">Cancel</a>';
			}

			function saveRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
				oTable.fnDraw();
			}

			function cancelEditRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnDraw();
			}

			var oTable = $('#editable-sample').dataTable({
				"aLengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"] // change per page values here
				],
				// set the initial value
				"iDisplayLength": 10,
				"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"oPaginate": {
						"sPrevious": "Prev",
						"sNext": "Next"
					}
				},
				"aoColumnDefs": [{
						'bSortable': true,
						'aTargets': [0]
					}
				]
			});

			jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall");

			var nEditing = null;

			$('#editable-sample_new').click(function (e) {
				e.preventDefault();

				if (oTable.fnGetData().length >= 50) {
					alert("You can not create more than 50 email addresses.");
					return;
				}

				var aiNew = oTable.fnAddData(['',
						'<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
				]);
				var nRow = oTable.fnGetNodes(aiNew[0]);
				editRow(oTable, nRow);
				nEditing = nRow;
			});

			$(document).on('click', '#editable-sample a.delete', function(e) {
				e.preventDefault();

				if (confirm("Are you sure to delete this row?") == false) {
					return;
				}

				var nRow = $(this).parents('tr')[0];

				$.ajax({
					url: "?action=delete&id=" + $(nRow).data('id'),
					type: "GET",
					success: function (result) {
						var msg = 'Email deleted successfully.';
						toastr.options.fadeOut = 3000;
						toastr.success(msg);
						oTable.fnDeleteRow(nRow);
					}
				}).fail( function() {
					var msg = 'Error occured and cannot delete. Please try again.';
					toastr.options.fadeOut = 3000;
					toastr.error(msg);
				});
			});

			$(document).on('click', '#editable-sample a.cancel', function(e) {
				e.preventDefault();
				if ($(this).attr("data-mode") == "new") {
					var nRow = $(this).parents('tr')[0];
					oTable.fnDeleteRow(nRow);
				} else {
					restoreRow(oTable, nEditing);
					nEditing = null;
				}
			});

			$(document).on('click', '#editable-sample a.edit', function(e) {
				e.preventDefault();
				var nRow = $(this).parents('tr')[0];
				if (nEditing !== null && nEditing != nRow) {
					restoreRow(oTable, nEditing);
					editRow(oTable, nRow);
					nEditing = nRow;
				} else if (nEditing == nRow && this.innerHTML == "Save") {
					saveRow(oTable, nEditing);
					nEditing = null;

					var url = "";
					var type = 'create';

					if ( $(nRow).data('id') ) {
						url = "?action=update&id=" + $(nRow).data('id');
						type = 'update';
					}
					else {
						url = "?action=create";
					}

					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: {
							email: $(nRow).find('td:first-child').html(),
							"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>"
						},
						success: function (result) {
							var msg = 'Email added successfully.';
							if (type == 'update') msg = 'Email updated successfully.'
							toastr.options.fadeOut = 3000;
							toastr.success(msg);
						}
					}).fail( function() {
						var msg = 'Error occured and can not save data. Please try again.';
						toastr.options.fadeOut = 3000;
						toastr.error(msg);
					});
				} else {
					editRow(oTable, nRow);
					nEditing = nRow;
				}
			});
		}
	};
}();

jQuery(document).ready(function() {
	EditableTable.init();
});
</script>

<?php include 'templates/footer.php'; ?>
