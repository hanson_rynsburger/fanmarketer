<?php
	include_once dirname(__FILE__) . '/./ASEngine/AS.php';
	if (! app('login')->isLoggedIn()) {
		redirect("login.php");
	}

	include_once 'profunctions.php';
	$db = getDbConnection();
	$currentUser = app('current_user');

	if ( $currentUser->email != '1337dang@gmail.com' ) {
		echo "
<script>
	window.location.href = '404.html';
</script>";
		exit;
	}

	include 'templates/header.php';
	$sidebarActive = 'messages';
	require 'templates/sidebar.php';

	var_dump($_POST);
	if (isset($_POST["news"])) {
		setOptionValue('news', trim($_POST["news"]));
	}
	if (isset($_POST["promotions"])) {
		setOptionValue('promotions', trim($_POST["promotions"]));
	}
?>

<link href="assets/summernote/dist/summernote.css" rel="stylesheet">

<section id="main-content">
	<section class="wrapper">
		<h1>Messages</h1>
		<div class="row">
			<div class="col-lg-12">
				<form method="post" action="">
					<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
					<div class="panel">
						<div class="panel-heading">
							<h2>News</h2>
						</div>
						<div class="panel-body">
							<textarea class="summernote" name="news">
								<?php echo getOptionValue('news'); ?>
							</textarea>
						</div>
						<div class="panel-footer">
							<input type="submit" class="btn btn-primary btn-lg" value="Save" />
						</div>
					</div>
				</form>
			</div>

			<div class="col-lg-12">
				<form method="post" action="">
					<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
					<div class="panel">
						<div class="panel-heading">
							<h2>Promotions</h2>
						</div>
						<div class="panel-body">
							<textarea class="summernote" name="promotions">
								<?php echo getOptionValue('promotions'); ?>
							</textarea>
						</div>
						<div class="panel-footer">
							<input type="submit" class="btn btn-primary btn-lg" value="Save" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
</section>

<script src="assets/summernote/dist/summernote.min.js"></script>
<script>
	jQuery(document).ready(function(){
		$('.summernote').summernote({
			height: 200,				 // set editor height
			minHeight: null,			 // set minimum height of editor
			maxHeight: null,			 // set maximum height of editor
			focus: true				 // set focus to editable area after initializing summernote
		});
	});
</script>
<?php include 'templates/footer.php'; ?>
