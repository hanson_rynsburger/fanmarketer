<div class="tab-pane active" id="setup">
	<form class="form-horizontal" role="form" method='post' action=''>
		<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
		<div class="form-group">
			<label class="col-lg-2 col-sm-2 control-label">Campaign Name</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="name" name = "name" value = '<?= $name;?>'>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 col-sm-2 control-label">Keyword 1</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="keyword1" name ="keyword1" value = '<?= $keyword1;?>'>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 col-sm-2 control-label">Keyword 2</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="keyword2" name = "keyword2" value = '<?= $keyword2;?>'>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 col-sm-2 control-label">Keyword 3</label>
			<div class="col-lg-10">
				<input type="text" class="form-control" id="keyword3" name = "keyword3" value = '<?= $keyword3;?>'>
			</div>
		</div>
		<? if (gotOto1($userid) == "YES") {
			echo '
			<div class="form-group">
				<label class="col-lg-2 col-sm-2 control-label">Keyword 4</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="keyword1" name ="keyword4" value = "'.$keyword4.'">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-sm-2 control-label">Keyword 5</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="keyword2" name = "keyword5" value = "'.$keyword5.'">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-sm-2 control-label">Keyword 6</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="keyword3" name = "keyword6" value = "'.$keyword6.'">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-sm-2 control-label">WP Plugin URL (<a href="https://s3.amazonaws.com/fanmarketer/fanmarketerplugin.zip">[DOWNLOAD PLUGIN]</a>)</label>
				<div class="col-lg-10">
					<input type="text" class="form-control" id="pluginurl" name = "pluginurl" value = "'.$pluginurl.'">
				</div>
			</div>';
		} ?>
		<div class="form-group">
			<label class="col-lg-2 col-sm-2 control-label">Page</label>
			<div class="col-lg-10">
			<?php
				//echo ' <select class="form-control" name ="fbpage"> ';
				$query = "me/accounts";
				//echo "access token =".$token;
				$response = FacebookQuery($query, "&limit=5000" , $token);

				echo "<select class = 'form-control' name = 'fbpage'>";
				foreach ($response ['data'] as $page){
					$pagename = $page['name'];
					$id = $page['id'];
					$id = trim($id);
					$pagetoken = $page['access_token'];

					if ($id == $fbpage) {
						echo "<option value = '".$id."' selected = 'selected'>".$pagename."</option>";
						$db->update('Campaigns', array ("pagetoken" =>$pagetoken ),
							"ID = :campaign",
							array("campaign" => $campaignID)
						);
					} else {
						echo "<option value = '".$id."'>".$pagename."</option>";
					}
				}
				echo "</select>";
			?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-lg-offset-2 col-lg-10">
				<button type="submit" class="btn btn-danger" name = "save">Save</button>
			</div>
		</div>
	</form>
</div>
