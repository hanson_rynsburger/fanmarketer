<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "lastRSS.php";
require "fm2YoutubeAPI.php";
require 'userselect.php';

$conn = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');


$sql =   "SELECT * FROM Campaigns WHERE   name != ' ' AND status = 'ON' ORDER BY RAND()";

foreach ($conn->query($sql) as $row) {
	$campaign  = $row['ID'];	
	$fbpage = $row['pageid'];   
	$viralApproval = $row['viralstatus'];   
	$token = $row['pagetoken'];
	$userid = $row['userid'];
	
	if  (checkTaskLog($userid, "a", 60, $conn)){		
		postViral($campaign, $conn,$viralApproval, $token,$fbpage);
		postFeed($campaign, $conn, $token,$fbpage);
		postVideo($campaign, $conn, $token,$fbpage, $userid);
		postCustom($campaign, $conn,$token,$fbpage);
	
		addToTaskLog($userid, time(), "a", $conn);
		break;
	}
}


$conn = null;


function postVideo($campaign, $conn, $token,$fbpage, $userid){

	
	$freq = getFrequency($campaign, $conn, "videosfreq");
	
	
	if (chooseToPost($freq)){	
	
		$sql =   "SELECT * FROM settings WHERE userid = $userid   ";
		foreach ($conn->query($sql) as $row) {
			$google = $row['googleid'];
		}	
		$youtube = new fm2Youtube(array('key' => $google));
		
		$sql =   "SELECT * FROM Videos WHERE campaign = $campaign ";
		foreach ($conn->query($sql) as $row) {
			$id = $row['id'];  		
			$type = $row['type'];
			$url = $row['URL'];
			
			if ($type == 'channel'){
				if (!empty($url)){	
						
					$channel = $youtube->getChannelFromURL($url);
					$id = $channel->id;
					$datetime= date("c", strtotime("$date"));
					$response = $youtube->searchChannelVideos('', $id, $datetime);		
					
					foreach ($response as $video){
					
						$thumbnail = $video->snippet->thumbnails->default->url; 
						$title = $video->snippet->title;
						$description =  $video->snippet->description;		
						$id = $video->id->videoId;	
						$link = 'http://youtu.be/'.$id;	
						
						if (checkHistory($conn, $campaign, $link, "identifier")){	
							$result = PostLink($title, $link, $token,$fbpage );
							print_r($result);
							$createdpost = $result["id"];									
							addToLog("Video",$createdpost , "", $link, $campaign, $conn);
							return;
						}	
						
					}	
				}
			}
				
		}		
			
	}	
}



function postViral($campaign, $conn, $viralApproval,$token,$fbpage){
	
	$freq = getFrequency($campaign, $conn, "viralfreq");
	
	if (chooseToPost($freq)){	
	
		if ($viralApproval == "ON"){
			
			$sql =   "SELECT * FROM Viral WHERE campaign = $campaign AND approved = 'ON'  ";
		}
		else	{
		
			$sql =   "SELECT * FROM Viral WHERE campaign = $campaign   ";
		}
		foreach ($conn->query($sql) as $post) {
			$id = $post['id'];  		
			$message = $post['message'];  				
			$picture = $post["picture"];	
			$fullpicture = $post["fullpicture"];	
			$type =  $post["type"];
			$object =  $post["object"];
			$postid =  $post["postid"];
			$link =  $post["link"];
			//echo $message." ".$type."<p>";		
			if ($post["deleted"] == "YES"){
			
			}
			else if (checkHistory($conn, $campaign, $postid, "identifier")){	
				if (!empty($link)){				
					$result = PostLink($message, $link, $token,$fbpage );
				}
				else{	
					$result = PostPhoto($message, $fullpicture, $token,$fbpage );
				}	
				print_r($result);
				$createdpost = $result["post_id"];
				$sql = "UPDATE Viral SET deleted = 'YES' WHERE id = $id ";					
				$conn->exec($sql);				
				addToLog("Viral",$createdpost , "", $postid, $campaign, $conn);
				return;
			}	
		}
		
	}	

}


function postFeed($campaign, $conn,$token,$fbpage){

	$freq = getFrequency($campaign, $conn, "feedsfreq");
	
	if (chooseToPost($freq)){			
		
		$sql =   "SELECT * FROM Feeds WHERE campaign = $campaign ";
		
		foreach ($conn->query($sql) as $row) {
			$url = $row['URL']; 			
			// Create lastRSS object
			$rss = new lastRSS;

			// Set cache dir and cache time limit (1200 seconds)
			// (don't forget to chmod cahce dir to 777 to allow writing)
			$rss->cache_dir = '';
			$rss->cache_time = 0;
			$rss->cp = 'US-ASCII';
			$rss->date_format = 'l';

			// Try to load and parse RSS file of Slashdot.org
			$rssurl = $url;

			if ($rs = $rss->get($rssurl)) {
				foreach ($rs['items'] as $item){
					$title = $item['title'];
					$link = $item['link'];
					if (checkHistory($conn, $campaign, $link, "identifier")){	
						$result = PostLink($title, $link, $token,$fbpage );
						print_r($result);
						$createdpost = $result["id"];									
						addToLog("Feed",$createdpost , "", $link, $campaign, $conn);
						return;
					}	
				
				}
			}
			else {
				echo "Error: It's not possible to get $rssurl...";
			}
			
			
		}
		
	}	

}
function postCustom($campaign, $conn,$token,$fbpage){

	$freq = getFrequency($campaign, $conn, "customfreq");
	
	if (chooseToPost($freq)){			
		
		$sql =   "SELECT * FROM Custom WHERE campaign = $campaign RAND() LIMIT 1 ";
		
		foreach ($conn->query($sql) as $custom) {
			$campaignID = $custom['campaign'];
			$customurl = $custom['URL'];  
			$customid =  $custom['id'];  
			$custommessage  =    $custom['message'];  
	
			
			$result = PostLink($custommessage, $customurl, $token,$fbpage );
			print_r($result);
			$createdpost = $result["id"];									
			addToLog("Promotion",$createdpost , "", $customurl, $campaign, $conn);
			return;					
			
			
		}
		
	}	

}




function PostPhoto($message, $link, $accesstoken,$fbpage ){	
		
		
		$data['message'] = $message;
		$data['url'] = $link ;
		$data['access_token'] = $accesstoken;
		
		$post_url = 'https://graph.facebook.com/'.$fbpage.'/photos';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $post_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode($result, TRUE);
		
		return $result;
	
	
	
}



function PostLink($message, $link, $accesstoken,$fbpage ){	
		
		
		$data['link'] = $link;
		$data['message'] = $message;
		$data['access_token'] = $accesstoken;
		$message = html_entity_decode($message);
		
		$post_url = 'https://graph.facebook.com/'.$fbpage.'/feed';
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $post_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$result = json_decode($result, TRUE);
		
		return $result;
	
	
	
}



function checkHistory($conn, $campaign, $identifiervalue, $identifiercolumn){

		$sql =   "SELECT * FROM PostLog WHERE  $identifiercolumn = '$identifiervalue' AND campaign = $campaign ";			
		$res = $conn->query($sql);		
		if ($res->fetchColumn() > 0){			
		
			return 0;
		}
		
		else{
			return 1;
		}	
}


function addToLog($source, $postid, $error, $identifier, $campaign, $conn){
		$time = time();
		$sql = "INSERT INTO PostLog (source, postid, timesent, error, identifier, campaign)
			VALUES ( '$source', '$postid', '$time', '$error', '$identifier', $campaign )";			
		//echo $sql."<p>";
		$conn->exec($sql);
		// print_r($conn->errorInfo());	
		return;

}

function getFrequency ($campaign, $conn, $type){

		$sql =   "SELECT $type FROM Campaigns WHERE ID = $campaign ";
		foreach ($conn->query($sql) as $row){
			$frequency = $row[$type];
			return $frequency;
		}
}

function chooseToPost($frequency){
	
	if ($frequency == 0){
		return 0;
	}
	
	$random = mt_rand(1,30);

	$powered = pow($frequency, 2);	
	
	if($random <= $powered+1){	
		
		return 1;
	}
	else{
		return 0;
	}
}





?>

