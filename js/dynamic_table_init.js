$.fn.dataTableExt.afnSortData['dom-checkbox'] = function  ( oSettings, iColumn )
{
    var aData = [];
    jq182( 'td:eq('+iColumn+') input', oSettings.oApi._fnGetTrNodes(oSettings) ).each( function () {
        aData.push( this.checked==true ? "1" : "0" );
    } ); // this only works fine with jquery 1.82. if higher version used, it returns in a weird sequence
    return aData;
}

function fnFormatDetails ( oTable, nTr )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>Rendering engine:</td><td>'+aData[1]+' '+aData[4]+'</td></tr>';
    sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
    sOut += '<tr><td>Extra info:</td><td>And any further details here (img etc)</td></tr>';
    sOut += '</table>';

    return sOut;
}

$(document).ready(function() {
    $('#dynamic-table').dataTable( {
    } );

    $('#video-table').dataTable( {
    } );

    $('#comments-table').dataTable( {
    } );

    $('#replies-table').dataTable( {
    } );

    $('#custom-table').dataTable( {
    } );

    $('#pages-table').dataTable( {
	   "iDisplayLength" : 100,
       "aoColumnDefs": [ {
            "aTargets": [ 2, 3 ],
            "mRender": function ( data, type, full ) {
                return type === 'display' && data == '999999999' ?
                'Custom' :
                data;
            }
        } ]
    } );

    $('#list-table').dataTable( {
        "iDisplayLength" : 100
    } );

    if ($('#viral-table').length > 0) {
        if ($('#viral-table th').length == 8) {
            $('#viral-table').dataTable( {
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "sSortDataType": "dom-checkbox" }
                ]
            } );
        }
        else {
            $('#viral-table').dataTable( {} );
        }
    }

    $('#images-table').dataTable( {
    } );

    $('#log-table').dataTable( {
        "aaSorting": [[ 2, "desc" ]],
	   "iDisplayLength" : 25
    } );
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement( 'th' );
    var nCloneTd = document.createElement( 'td' );
    nCloneTd.innerHTML = '<img src="img/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each( function () {
        this.insertBefore( nCloneTh, this.childNodes[0] );
    } );

    $('#hidden-table-info tbody tr').each( function () {
        this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    } );

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[1, 'asc']]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $(document).on('click','#hidden-table-info tbody td img',function () {
        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "img/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "img/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
        }
    } );
} );
