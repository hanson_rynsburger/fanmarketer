<?php
$db = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');

$campaign = $_POST['campaign'];

if (isset($_POST['addcustom'])) {
	$customurl= trim($_POST['customurl']);
	$custommessage= trim($_POST['custommessage']);

	$stmt = $db->prepare("INSERT INTO Custom (URL, campaign, message) VALUES (:url, :campaign, :msg)");
	$stmt->execute(array(
		"url"		=> $customurl,
		"campaign"	=> $campaign,
		"msg"		=> $custommessage
	));

	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=custom';

	echo '
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addimage'])) {
	$imageurl= trim($_POST['imageurl']);
	$imagename= trim($_POST['imagename']);

	$stmt = $db->prepare("INSERT INTO Images (URL, name, campaign) VALUES (:url, :name, :campaign)");
	$stmt->execute(array(
		"url"		=> $imageurl,
		"name"		=> $imagename,
		"campaign"	=> $campaign
	));

	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=images';

	echo '
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addfeed'])) {
	$feedurl= trim($_POST['feedurl']);

	// $sql = "INSERT INTO Feeds (URL, campaign)
	// VALUES ('".$feedurl."', '".$campaign."')";

	$stmt = $db->prepare("INSERT INTO Feeds (URL, campaign) VALUES (:url, :campaign)");
	$stmt->execute(array(
		"url"		=> $feedurl,
		"campaign"	=> $campaign
	));

	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=feeds';

	echo '
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addvideo'])) {
	$videourl= trim($_POST['videourl']);
	$videotype =  trim($_POST['videotype']);

	// $sql = "INSERT INTO Videos (URL, type, campaign)
	// VALUES ('".$videourl."', '".$videotype."' , '".$campaign." ')";

	$stmt = $db->prepare("INSERT INTO Videos (URL, type, campaign) VALUES (:url, :type, :campaign)");
	$stmt->execute(array(
		"url"		=> $videourl,
		"type"		=> $videotype,
		"campaign"	=> $campaign
	));

	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=video';
		echo'
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addcomment'])) {

	$comment= trim($_POST['comment']);
	// $comment = addslashes($comment);

	// $sql = "INSERT INTO Comments (comment, campaign)
	// VALUES (' ".$comment."', '".$campaign." ')";

	$stmt = $db->prepare("INSERT INTO Comments (comment, campaign) VALUES (:comment, :campaign)");
	$stmt->execute(array(
		"comment"	=> $comment,
		"campaign"	=> $campaign
	));

	// $db->exec($sql);
	// $db = null;
	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=comments';

	echo'
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addreply'])) {
	$reply= trim($_POST['reply']);
	// $reply = addslashes($reply);

	// $sql = "INSERT INTO Replies (reply, campaign)
	// VALUES (' ".$reply."', '".$campaign." ')";

	$stmt = $db->prepare("INSERT INTO Replies (reply, campaign) VALUES (:reply, :campaign)");
	$stmt->execute(array(
		"reply"		=> $reply,
		"campaign"	=> $campaign
	));

	// $db->exec($sql);
	// $db = null;
	$redirect = 'editcampaign.php?campaign='.$campaign.'&view=replies';

	echo'
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}

if (isset($_POST['addpage'])) {
	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

	$stmt = $db->prepare("INSERT INTO Pages (campaign, name, pageid, likes, talkingabout, supress, supress2) VALUES (:campaign, :name, :pageid, :likes, :talkingabout, :supress, :supress2)");

	$stmt->execute(array(
		'campaign'		=> trim($_POST['campaign']),
		'name'			=> trim($_POST['name']),
		'pageid'		=> trim($_POST['pageid']),
		'likes'			=> 999999999,
		'talkingabout'	=> 999999999,
		'supress'		=> 'OFF',
		'supress2'		=> 'OFF'
	));

	$redirect = 'editcampaign.php?campaign=' . $campaign . '&view=pages';

	echo '
		<script>
			window.location.href = "'.$redirect.'";
		</script>
	';
}
?>
