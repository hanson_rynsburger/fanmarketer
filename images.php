

	<form class="form-horizontal" role="form" method='post' action='add.php'>
	      <input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
	      
	    <input type="hidden" name="campaign" value="<?= $campaignID ?>" />	
	    <input type="hidden" name="slidervalue" value="<?= getSliderValue($campaignID, "imagesfreq"); ?>" id = "imagesfreq" />		    
		<div class="form-group">
			<label  class="col-lg-2 col-sm-2 control-label">Frequency: </label>
                                     <div class="col-lg-4">
                                           <div id="imagesslider" class="slider"></div>
						<div class="slider-info">					 
							<span id="imagesslider-amount"><?= getSliderValueLabel(getSliderValue($campaignID, "imagesfreq")); ?></span>
						</div>
                                 </div>
			  <div class="col-lg-6">	
			  </div>
		</div>	    
		
		<div class="form-group">
		      <label  class="col-lg-2 col-sm-2 control-label">User ID</label>
		      <div class="col-lg-8">
			  <input type="text" class="form-control" id="imageurl"  name = "imageurl" >
			   
		      </div>
		   </div>
		 <div class="form-group">
		      <label  class="col-lg-2 col-sm-2 control-label">URL</label>
		      <div class="col-lg-8">
			  <input type="text" class="form-control" id="imagename"  name = "imagename" >
			   
		      </div>		     
		         <div class="col-lg-2">
				  <button type="submit" class="btn btn-danger" name = "addimage">Add</button>
		      </div>
		  </div>		
	      </form>

	<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="images-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>URL</th>			
					<th>Actions</th>				
				</tr>
			</thead>
			<tbody>
			<?php	

			$query = "SELECT * FROM Images WHERE  campaign =  ".$campaignID." ";
	
			$result = $db->select($query);
			
			foreach ($result as $image){
				$campaignID = $image['campaign'];
				$url = $image['URL'];  
				$imagesid =  $image['id'];  
				$name = $image['name'];
				$delete = '<a href = "delete.php?type=Images&id='.$imagesid.'&campaign='.$campaignID.'">Delete</a>';
				
				echo '<tr class="gradeA">
						<td>'.$url.'</td>
						<td>'.$name.'</td>
						<td>'.$delete.'</td>
				</tr>';								
			}
			
			
			?>
				
		
			<tfoot>
				<tr>
					<th>Username</th>
					<th>URL</th>		
					<th>Actions</th>	
				</tr>
			</tfoot>
		</table>
	</div>	
