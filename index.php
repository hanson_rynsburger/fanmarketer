<?php
include 'templates/header.php';
include 'profunctions.php';
$userid = ASSession::get('user_id');
$oto1 = gotOto1($userid);

// Include sidebar template
// and set active page to "home".
$sidebarActive = 'home';
require 'templates/sidebar.php';
?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-6">
        <section class="panel">
          <div class="panel-body">
    				<img src = 'logo.png'>
    				<h1>Welcome to Fan Marketer</h1>
    				<h4>To get started please watch the <a href ='tutorials.php'>Tutorials</a> and follow the instructions.</h4>
    				<h4>Any problems? Contact us at <a href ='http://bytemarketer.zendesk.com/'>Support</a>.</h4>
  		    </div>
  		  </section>

        <section class="panel">
          <header class="panel-heading">
            News
          </header>
          <div class="panel-body">
            <?php
              echo getOptionValue('news');
            ?>
          </div>
        </section>
      </div>
      <div class="col-lg-6">
        <?php $pcontent = trim(getOptionValue('promotions'));
        if (!empty($pcontent)) { ?>
        <section class="panel">
          <header class="panel-heading">
            Fan Marketer Recommends
          </header>
          <div class="panel-body">
            <?php
              echo $pcontent;
            ?>
          </div>
        </section>
        <?php } ?>
        <? if ($oto1 != "YES") {
        echo '
          <section class="panel">
            <header class="panel-heading">
              Upgrade to PRO Version ?
            </header>
            <div class="panel-body">
              <p>Want to go PRO? Get UNLIMITED Campaigns and AMAZING new features.
              <p><strong>Checkout the benfits and upgrade to Fan Marketer PRO  <a href ="http://fanmarketer.com/sales/oto1.html">HERE</a></strong>
            </div>
          </section>
        ';} ?>
      </div>
    </div>

  </section>
</section>

<!--main content end-->

<?php include 'templates/footer.php'; ?>
<script src="ASLibrary/js/index.js"></script>
</body>
</html>
