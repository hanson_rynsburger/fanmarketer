<?php
$campaignId = $_GET['campaignid'];
$postId = $_GET['postid'];

$conn = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM Campaigns WHERE id=$campaignId";
try {
	foreach ($conn->query($sql) as $row) {
		$fbpage = $row['pageid'];
		$token = $row['pagetoken'];
		$userid = $row['userid'];
	}
}
catch ( PDOException $pe) {
	var_dump($pe);
}

$emailaddrs = [];
$next = '';

do {
	$data = fmGetComments($postId, $token, $next);
	foreach( $data["data"] as $dt ) {
		$et = extract_email_address($dt["message"]);
		if ( $et ) $emailaddrs = array_merge($emailaddrs, $et);
	}
	$next = $data["paging"]["cursors"]["after"];
} while ( isset($data["paging"])
	&& isset($data["paging"]["cursors"])
	&& isset($data["paging"]["cursors"]["after"] ) );

header('Content-type: application/csv');
header('Content-Disposition: attachment; filename="emails-'.$campaignId.'.csv"');
echo "Email Address\n";
echo implode(array_values($emailaddrs), "\n");

function fmGetComments($postid, $token, $next) {
	$url = 'https://graph.facebook.com/' . $postid . '/comments?access_token=' . $token;
	if ( !empty($next) ) {
		$url .= '&after=' . $next;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, TRUE);

	return $result;
}

function extract_email_address($string) {
    foreach(preg_split('/\s/', $string) as $token) {
        $email = filter_var(filter_var($token, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
        if ($email !== false) {
            $emails[] = $email;
        }
    }
    return $emails;
}
?>
