<form class="form-horizontal" role="form" method='post' action='getviral.php'>
	<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
	<input type="hidden" name="campaign" value="<?= $campaignID ?>" />
	<input type="hidden" name="slidervalue" value="<?= getSliderValue($campaignID, "viralfreq"); ?>" id = "viralfreq" />

	<div class="form-group">
		<label class="col-lg-2 col-sm-2 control-label">Frequency: </label>
		<div class="col-lg-4">
			<div id="viralslider" class="slider"></div>
			<div class="slider-info">
				<span id="viralslider-amount"><?= getSliderValueLabel(getSliderValue($campaignID, "viralfreq")); ?></span>
			</div>
		</div>
		<div class="col-lg-6">
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-6">
			<button type="submit" class="btn btn-danger" name = "fetchviral">Manual Fetch</button>
		</div>
	</div>

	<div class="form-group">
		<div class="col-lg-6">
			Require manual approval?
			<?php
				if ($viralApproval == "ON") {
					echo '<input type="checkbox" checked="" data-toggle="switch" onchange="changeViralStatus(\''.$campaignID.'\');" />';
				} else {
					echo '<input type="checkbox" data-toggle="switch" onchange="changeViralStatus(\''.$campaignID.'\');" />';
				}
			?>

		</div>
	</div>
</form>

<div class="adv-table">
	<table class="display table table-bordered table-striped" id="viral-table">
		<thead>
			<tr>
				<th>No</th>
				<th>Post ID</th>
				<th>Picture</th>
				<th>Message</th>
				<th>Page</th>
				<th>Type</th>
				<th>Likes</th>
				<th>Delete</th>
				<? if ($viralApproval == "ON") {
					echo "<th>Approve ?</th>";
				}
				?>
			</tr>
		</thead>
		<tbody>
		<?php
			$query1 = "SELECT VV.* FROM (SELECT * FROM Viral WHERE Viral.`campaign` = $campaignID and Viral.`deleted` is null and approved='ON') AS VV INNER JOIN (SELECT * FROM Pages WHERE Pages.`campaign` = $campaignID AND Pages.talkingabout >= 100 AND Pages.`supress2`!='ON') PP ON VV.pageid = PP.pageid order by VV.id desc";

			$query2 = "SELECT VV.* FROM (SELECT * FROM Viral WHERE Viral.`campaign` = $campaignID and Viral.`deleted` is null and (approved!='ON' or approved is null)) AS VV INNER JOIN (SELECT * FROM Pages WHERE Pages.`campaign` = $campaignID AND Pages.talkingabout >= 100 AND Pages.`supress2`!='ON') PP ON VV.pageid = PP.pageid order by VV.id desc LIMIT 50";

			$result1 = $db->select($query1);
			$result2 = $db->select($query2);

			$result = array_slice(array_merge($result1, $result2), 0, 50);
			$idx = 0;

			foreach ($result as $post) {
				$id = $post['id'];
				$message = mb_convert_encoding($post['message'], 'Windows-1252', 'utf-8');
				if (mb_strlen($message) >100) {
					$message = mb_substr($message, 0, 100);
					$message.= "...";
				}
				$pageid = $post['pageid'];
				$picture = $post["picture"];
				$type = $post["type"];

				$likes = $post['likes'];
				$postid = $post['postid'];
				$approvalstatus = $post['approved'];
				$delete = "./deleteviral.php?id=".$id."&campaign=".$campaignID."";
				$deleted = $post['deleted'];

				$query = "SELECT name FROM Pages WHERE campaign = " . $campaignID . " AND pageid = '" . $pageid . "'";
				$result = $db->select($query);

				foreach ($result as $row) {
					$pagename = mb_convert_encoding($row["name"], 'Windows-1252', 'utf-8');
				}

				$idx ++;
				echo '<tr class="gradeA">
					<td title="' . $id . '">' . $idx . '</td>
					<td><a href ="http://facebook.com/'.$postid.'">'.$postid.'</a></td>
					<td><img src ="'.$picture.'" /></td>
					<td>'.$message.'</td>
					<td><a href ="http://facebook.com/'.$pageid.'">'.$pagename.'</a></td>
					<td>'.$type.'</td>
					<td>'.$likes.'</td>
					<td><a href ="'.$delete.'">Delete</a></td>';

				if ($viralApproval == "ON") {
					if ($approvalstatus == "ON") {
						echo' <td><p hidden>A</p><input type="checkbox" checked="" onchange = "changeApprovalStatus(\''.$postid.'\', \''.$campaignID.'\');" /></td>';
					}
					else {
						echo '<td><p hidden>B</p><input type="checkbox" onchange = "changeApprovalStatus(\''.$postid.'\', \''.$campaignID.'\');" /></td>';
					}
				}
				echo'</tr>';

			}
		?>

		<tfoot>
			<tr>
				<th>Id</th>
				<th>Post ID</th>
				<th>Picture</th>
				<th>Message</th>
				<th>Page</th>
				<th>Type</th>
				<th>Likes</th>
				<th>Delete</th>
				<? if ($viralApproval == "ON") {
					echo "<th>Approve ?</th>";
				} ?>
			</tr>
		</tfoot>
	</table>
</div>
