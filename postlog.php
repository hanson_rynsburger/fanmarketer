<div class="adv-table">
	<table  class="display table table-bordered table-striped" id="log-table">
		<thead>
			<tr>
				<th>Post Created</th>
				<th>Source</th>
				<th>Time</th>
			</tr>
		</thead>
		<tbody>
		<?php
$query = "SELECT * FROM PostLog WHERE  campaign =  " . $campaignID . " ORDER BY id DESC LIMIT 200";
$result = $db->select($query);

foreach ($result as $row) {
	$postid = $row['postid'];

	if (preg_match('/^\d+_\d+$/', $postid)) {
		$postcreated = 'http://facebook.com/' . $postid;
	} else {
		$postcreated = $postid;
		$matches=[];
		if (preg_match('/(https?:\/\/)?(www.)?((facebook.com)|(fb.me))\/(.*)/i', $postid, $matches)) {
			if ($matches[6]) {
				$postid = $matches[6];
			}
		}
	}

	$source = $row['source'];
	$time = $row['timesent'];
	$time = date("Y-m-d H:i", $time);

	echo '<tr class="gradeA">
					<td><a href="' . $postcreated . '">' . $postid . '</a></td>
					<td>' . $source . '</td>
					<td>' . $time . '</td>
			</tr>';
}
?>
		<tfoot>
			<tr>
				<th>Post Created</th>
				<th>Source</th>
				<th>Time</th>
			</tr>
		</tfoot>
	</table>
</div>
