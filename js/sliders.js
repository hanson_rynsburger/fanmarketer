function getSliderFrequency(value, table) {

    var campaign = url('?campaign');

    request = jQuery.ajax({
        type: 'post',
        url: 'setfrequency.php',
        data: { value: value, table: table, campaign: campaign }
    })
    .done(function(msg) {
        //alert(msg);
    });

    if (value == 0) {
        return "Never";
    }
    if (value == 1) {
        return "Much Less Often";
    }
    if (value == 2) {
        return "Less Often";
    }
    if (value == 3) {
        return "Standard";
    }
    if (value == 4) {
        return "More Often";
    }
    if (value == 5) {
        return "Much More Often";
    }
}


var Sliders = function() {

    // default sliders
    $("#default-slider").slider();

    // snap inc
    $("#snap-inc-slider").slider({
        value: $("#videosfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#snap-inc-slider-amount").text(getSliderFrequency(ui.value, "Videos"));
        }
    });

    // snap inc
    $("#slider").slider({
        value: $("#feedsfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#slider-amount").text(getSliderFrequency(ui.value, "Feeds"));
        }
    });
    // snap inc
    $("#commentsslider").slider({
        value: $("#commentsfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#commentsslider-amount").text(getSliderFrequency(ui.value, "Comments"));
        }
    });

    // snap inc
    $("#customsslider").slider({
        value: $("#customsfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#customsslider-amount").text(getSliderFrequency(ui.value, "Custom"));
        }
    });

    // snap inc
    $("#repliesslider").slider({
        value: $("#repliesfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#repliesslider-amount").text(getSliderFrequency(ui.value, "Replies"));
        }
    });

    // snap inc
    $("#viralslider").slider({
        value: $("#viralfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#viralslider-amount").text(getSliderFrequency(ui.value, "Viral"));
        }
    });

    $("#imagesslider").slider({
        value: $("#imagesfreq").val(),
        min: 0,
        max: 5,
        step: 1,
        slide: function(event, ui) {
            $("#imagesslider-amount").text(getSliderFrequency(ui.value, "Images"));
        }
    });

    // $("#snap-inc-slider-amount").text("Standard");

    // range slider
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function(event, ui) {
            $("#slider-range-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });

    $("#slider-range-amount").text("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

    //range max
    $("#slider-range-max").slider({
        range: "max",
        min: 1,
        max: 10,
        value: 2,
        slide: function(event, ui) {
            $("#slider-range-max-amount").text(ui.value);
        }
    });

    $("#slider-range-max-amount").text($("#slider-range-max").slider("value"));

    // range min
    $("#slider-range-min").slider({
        range: "min",
        value: 37,
        min: 1,
        max: 700,
        slide: function(event, ui) {
            $("#slider-range-min-amount").text("$" + ui.value);
        }
    });

    $("#slider-range-min-amount").text("$" + $("#slider-range-min").slider("value"));

    //
    // setup graphic EQ
    $("#eq > span").each(function() {
        // read initial values from markup and remove that
        var value = parseInt($(this).text(), 10);
        $(this).empty().slider({
            value: value,
            range: "min",
            animate: true,
            orientation: "vertical"
        });
    });

    // bound to select
    var select = $("#minbeds");
    if (select.length > 0) {
        var slider = $("<div id='slider'></div>").insertAfter(select).slider({
            min: 1,
            max: 6,
            range: "min",
            value: select[0].selectedIndex + 1,
            slide: function(event, ui) {
                select[0].selectedIndex = ui.value - 1;
            }
        });

        $("#minbeds").change(function() {
            slider.slider("value", this.selectedIndex + 1);
        });
    }

    // bound to select
    /* var select = $("#feedsfreq");
    if (select.length > 0) {
        var slider = $("<div id='feedsslider'></div>").insertAfter(select).slider({
            min: 1,
            max: 6,
            range: "min",
            value: select[0].selectedIndex + 1,
            slide: function(event, ui) {
                select[0].selectedIndex = ui.value - 1;
            }
        });
        $("#feedsfreq").change(function() {
            slider.slider("value", this.selectedIndex + 1);
        });
    } */

    // bound to select
    var select = $("#videofreq");
    if (select.length > 0) {
        var slider = $("<div id='videoslider'></div>").insertAfter(select).slider({
            min: 1,
            max: 6,
            range: "min",
            value: select[0].selectedIndex + 1,
            slide: function(event, ui) {
                select[0].selectedIndex = ui.value - 1;
            }
        });
        $("#videofreq").change(function() {
            slider.slider("value", this.selectedIndex + 1);
        });
    }

    // vertical slider
    $("#slider-vertical").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 100,
        value: 60,
        slide: function(event, ui) {
            $("#slider-vertical-amount").text(ui.value);
        }
    });
    $("#slider-vertical-amount").text($("#slider-vertical").slider("value"));

    // vertical range sliders
    $("#slider-range-vertical").slider({
        orientation: "vertical",
        range: true,
        values: [17, 67],
        slide: function(event, ui) {
            $("#slider-range-vertical-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });

    $("#slider-range-vertical-amount").text("$" + $("#slider-range-vertical").slider("values", 0) + " - $" + $("#slider-range-vertical").slider("values", 1));
}();
