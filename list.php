<form class="form-horizontal" role="form" method='post' action='getPosts.php'>
	<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
	<input type="hidden" name="campaign" value="<?= $campaignID ?>" />

	<div class="form-group">
		<div class="col-lg-10">
			<button type="submit" class="btn btn-danger" name = "test">Fetch Posts</button>
		</div>
	</div>
</form>

<div class="adv-table">
	<table  class="display table table-bordered table-striped" id="list-table">
		<thead>
			<tr>
				<th>Post ID</th>
				<th>Message</th>
				<th>Type</th>
				<th>Created</th>
				<th>Get Leads?</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$query = "SELECT * FROM Posts WHERE  campaign =  ".$campaignID." ORDER BY created";
		$result = $db->select($query);

		foreach ($result as $post){
			$campaignID = $post['campaign'];
			$postid = $post['postid'];
			$message = $post['message'];
			$type  = $post['type'];
			$created  = $post['created'];
			$created = date("Y-m-d H:i", $created);

			echo '<tr class="gradeA">
					<td><a href ="http://facebook.com/'.$postid.'">'.$postid.'</a></td>
					<td>'.$message.'</td>
					<td>'.$type.'</td>
					<td>'.$created.'</td>
					<td><a href = "extractleads.php?campaignid='.$campaignID.'&postid='.$postid.'"><button class="btn btn-danger">Extract Leads</button></td>';
			echo '</tr>';
		}
		?>
		<tfoot>
			<tr>
				<th>Post ID</th>
				<th>Message</th>
				<th>Type</th>
				<th>Created</th>
				<th>Get Leads?</th>
			</tr>
		</tfoot>
	</table>
</div>
