<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include 'templates/header.php';
require_once __DIR__ . '/vendor/facebook/graph-sdk/src/Facebook/autoload.php';
include 'profunctions.php';

$db = app('db');
$userid = ASSession::get('user_id');

$result = $db->select(
	"SELECT * FROM `settings` WHERE `userid` = :id",
	array("id" => $userid)
);

foreach ($result as $user) {
	$fbid = $user['fbid'];
	$secret = $user['fbsecret'];
	$google = $user['googleid'];
	$timezone = $user['timezone'];
	$flickrkey = $user['flickrkey'];
	$flickrsecret = $user['flickrsecret'];
	$reportemail = $user['reportemail'];
}

$redirectURL = 'https://fmarketer.me/facebookcallback.php?userid=' . $userid;
// .'&fbid='.$fbid.'&secret='.$secret;

if (isset($_POST['save'])) {
	$fbid = trim($_POST['FBKey']);
	$secret = trim($_POST['FBSecret']);
	$google = trim($_POST['GoogleKey']);
	$timezone = trim($_POST['timezone']);
	$flickrkey = trim($_POST['flickrkey']);
	$flickrsecret = trim($_POST['flickrsecret']);
	$reportemail = trim($_POST['reportemail']);

	if (!empty($result)) {
		$db->update('settings',
			array("fbid" => $fbid, "fbsecret" => $secret, "flickrkey" => $flickrkey, "flickrsecret" => $flickrsecret, "googleid" => $google, "timezone" => $timezone, 'reportemail' => $reportemail),
			"userid = :id",
			array("id" => $userid)
		);
	} else {
		$db->insert('settings', array(
			"userid" => $userid,
			"fbid" => $fbid,
			"fbsecret" => $secret,
			"googleid" => $google,
			"flickrkey" => $flickrkey,
			"flickrsecret" => $flickrsecret,
			"timezone" => $timezone,
			"reportemail" => $reportemail,
		));
		print_r($db->errorInfo());
	}

	$fb = new \Facebook\Facebook([
		'app_id' => $fbid,
		'app_secret' => $secret,
		'default_graph_version' => 'v2.10',
	]);

	$helper = $fb->getRedirectLoginHelper();
	$permissions = ['publish_actions,manage_pages,publish_pages']; // optional
	$loginUrl = $helper->getLoginUrl('https://fmarketer.me/facebookcallback.php?userid=' . $userid, $permissions);
}
?>

<div class="row">
	<?php
// Include sidebar template
// and set active page to "home".
$sidebarActive = 'settings';
require 'templates/sidebar.php';
?>

	<!--main content start-->
	<section id="main-content">
		<section class="wrapper">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
						Settings
					</header>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method='post' action=''>
							<input type="hidden" name="<?=ASCsrf::getTokenName()?>" value="<?=ASCsrf::getToken()?>" />
								<div class="form-group">
									<label class="col-lg-2 control-label">Facebook App Key</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="FBKey" name = "FBKey" value = '<?=$fbid;?>'>
										<div class="text-muted">
											Copy following URL to your <code>Valid OAuth Redirect URIs</code> of Facebook APP - Facebook Login panel: <code><?=$redirectURL;?></code>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Facebook App Secret</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="FBSecret" name ="FBSecret" value = '<?=$secret;?>'>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Google API Key</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="GoogleKey" name = "GoogleKey" value = '<?=$google;?>'>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Timezone</label>
									<div class="col-lg-10">
										<select class="form-control m-bot15" name = "timezone">
										<?php
for ($i = 12; $i > 0; $i--) {
	if ($timezone == "-" . $i) {
		echo '<option value= -' . $i . ' selected = "selected">UTC -' . $i . ':00</option>';
	} else {
		echo '<option value= -' . $i . ' >UTC -' . $i . ':00</option>';
	}
}
for ($i = 0; $i < 13; $i++) {
	if ($timezone == "+" . $i) {
		echo '<option value= +' . $i . ' selected = "selected">UTC +' . $i . ':00</option>';
	} else {
		echo '<option value= +' . $i . ' >UTC +' . $i . ':00</option>';
	}
}
?>
										</select>
									</div>
								</div>
								<?
if (gotOto1($userid) == "YES") {
	echo '
									<div class="form-group">
										<label class="col-lg-2 control-label">Report Email (leave black to disable emails)</label>
										<div class="col-lg-10">
											<input type="text" class="form-control" id="reportemail" name = "reportemail" value = "' . $reportemail . '">
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 control-label">Flickr Key</label>
										<div class="col-lg-10">
											<input type="text" class="form-control" id="flickrkey" name = "flickrkey" value = "' . $flickrkey . '">
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-2 control-label">Flickr Secret</label>
										<div class="col-lg-10">
											<input type="text" class="form-control" id="flickrsecret" name = "flickrsecret" value = "' . $flickrsecret . '">
										</div>
									</div>';
}?>

								<div class="form-group">
									<label class="col-lg-2 control-label">Domain</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" value='fmarketer.me' readonly>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">URL</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" value='https://fmarketer.me' readonly>
									</div>
								</div>

							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-danger" name = "save">Save</button>
									<?
if (!empty($loginUrl)) {
	echo "<a href = '" . $loginUrl . "'>Auth Facebook</a>";
}
?>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</section>
 	</section>

	<!--main content end-->

	<?php include 'templates/footer.php';?>
	<script src="ASLibrary/js/index.js"></script>
	</body>
</html>
