<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
set_time_limit(30);
require "lastRSS.php";
require "fm2YoutubeAPI.php";
require 'userselect.php';
require '../profunctions.php';
require "../phpFlickr.php";

function doStart() {
	$conn = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');

	// $sql = "SELECT * FROM Campaigns WHERE name != ' ' AND status = 'ON' ORDER BY RAND()";
    $sql = "SELECT CC.* FROM Campaigns CC INNER JOIN as_users au ON CC.`userid` = au.`user_id` WHERE CC.name != ' ' AND CC.status = 'ON' AND au.domain = '${_SERVER['SERVER_NAME']}' ORDER BY RAND()";

	foreach ($conn->query($sql) as $row) {
		$campaign = $row['ID'];
		$fbpage = $row['pageid'];
		$viralApproval = $row['viralstatus'];
		$token = $row['pagetoken'];
		$userid = $row['userid'];
		$wp_url = $row['pluginurl'];

		if (checkTaskLog($userid, "a", 60, $conn)) {
			addToTaskLog($userid, time(), "a", $conn);
			postFeed($campaign, $conn, $token, $fbpage, $userid, $wp_url);
			postVideo($campaign, $conn, $token, $fbpage, $userid, $wp_url);
			postCustom($campaign, $conn, $token, $fbpage);
			postViral($campaign, $conn, $viralApproval, $token, $fbpage);

			if (gotOto1($userid) == "YES") {
				postImage($campaign, $conn, $token, $fbpage, $userid);
			}

			break;
		}
	}

	$conn = null;
}

function postImage($campaign, $conn, $token,$fbpage, $userid) {
	$freq = getFrequency($campaign, $conn, "imagesfreq");

	if (chooseToPost($freq)) {
		$sql = "SELECT * FROM settings WHERE userid = $userid ";
		foreach ($conn->query($sql) as $row) {
			$flickrkey = $row['flickrkey'];
			$flickrsecret = $row['flickrsecret'];
		}

		$sql = "SELECT * FROM Images WHERE campaign = $campaign ORDER BY RAND()";
		foreach ($conn->query($sql) as $row) {
			$id = $row['id'];
			$userid = $row['URL'];
			$params = array("extras"=> "url_o");
			$f = new phpFlickr($flickrkey);

			$result = $f->people_getPhotos($userid);
			$i = 0;
			foreach ($result as $row) {
				$photos = $row["photo"];
				foreach ($photos as $photo) {
					//print_r($photo);
					$id = $photo['id'];
					$title = $photo['title'];
					$farm = $photo['farm'];
					$secret = $photo['secret'];
					$server = $photo['server'];

					$image = 'https://farm'.$farm.'.staticflickr.com/'.$server.'/'.$id.'_'.$secret.'.jpg';

					if (checkHistory($conn, $campaign, $image, "identifier")) {
								$result = PostPhoto($title, $image, $token,$fbpage );
								print_r($result);
								$createdpost = $result["id"];
								addToLog("Photo",$createdpost ,"", $image, $campaign, $conn);
								return;
					}
				}
			}

			return $result;
		}
	}
}

function postVideo($campaign, $conn, $token, $fbpage, $userid, $wp_url) {
	$freq = getFrequency($campaign, $conn, "videosfreq");

	if (chooseToPost($freq)) {

		$sql = "SELECT * FROM settings WHERE userid = $userid ";
		foreach ($conn->query($sql) as $row) {
			$google = $row['googleid'];
		}
		if (!empty($google)) {
			$youtube = new fm2Youtube(array('key' => $google));

			$sql = "SELECT * FROM Videos WHERE campaign = $campaign  ORDER BY RAND() ";
			foreach ($conn->query($sql) as $row) {
				$id = $row['id'];
				$type = $row['type'];
				$url = $row['URL'];

				if ($type == 'channel') {
					if (!empty($url)) {
						$channel = $youtube->getChannelFromURL($url);
						$id = $channel->id;
						$datetime= date("c", strtotime("$date"));
						$response = $youtube->searchChannelVideos('', $id, $datetime);

						foreach ($response as $video) {
							$thumbnail = $video->snippet->thumbnails->default->url;
							$title = $video->snippet->title;
							$description = $video->snippet->description;
							$id = $video->id->videoId;
							$link = 'http://youtu.be/'.$id;
							$chosenvideo = $link;
							if (checkHistory($conn, $campaign, $link, "identifier")) {
								if (!empty($wp_url)) {
									$link = postToWP($wp_url, '', $title, $description, $link, 'video');
								}

								$result = PostLink($title, $link, $token, $fbpage );
								print_r($result);
								$createdpost = $result["id"];
								addToLog("Video", $createdpost, "", $chosenvideo, $campaign, $conn);
								return;
							}
						}
					}
				} else if ($type == 'playlist') {
					if (!empty($url)) {
						$playlist = $youtube->getPlaylistFromURL($url);
						$response =  $youtube->getPlaylistItemsByPlaylistId($playlist);

						// $hashtags = $row->hashtags;
						// $hashtags = explode(',', $hashtags);
						// $hashtag = $hashtags[array_rand($hashtags)];

						foreach ($response as $video) {
							$id = $video->contentDetails->videoId;
							$title = $video->snippet->title;
							$link = 'http://youtu.be/'.$id;
							$chosenvideo = $link;
							if (checkHistory($conn, $campaign, $link, "identifier")) {
								if (!empty($wp_url)) {
									$description ='<iframe width="640" height="360" src="//www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';

									$link = postToWP($wp_url, '', $title, $description, $link, 'video');
								}

								$result = PostLink($title, $link, $token, $fbpage );
								print_r($result);
								$createdpost = $result["id"];
								addToLog("Video", $createdpost, "", $chosenvideo, $campaign, $conn);

								return;
							}
						}
					}
				}
			}
		}
	}
}

function postViral($campaign, $conn, $viralApproval,$token,$fbpage) {
	return;

	$freq = getFrequency($campaign, $conn, "viralfreq");

	if (chooseToPost($freq)) {
		if ($viralApproval == "ON") {
			$sql = "SELECT VV.* FROM (SELECT * FROM Viral WHERE Viral.`campaign` = $campaign and Viral.`deleted` is null and approved='ON') AS VV INNER JOIN (SELECT * FROM Pages WHERE Pages.`campaign` = $campaign AND Pages.talkingabout >= 100 AND Pages.`supress2`!='ON') PP ON VV.pageid = PP.pageid order by rand()";
		} else {
			$sql = "SELECT VV.* FROM (SELECT * FROM Viral WHERE Viral.`campaign` = $campaign and Viral.`deleted` is null) AS VV INNER JOIN (SELECT * FROM Pages WHERE Pages.`campaign` = $campaign AND Pages.talkingabout >= 100 AND Pages.`supress2`!='ON') PP ON VV.pageid = PP.pageid order by rand()";
		}

		foreach ($conn->query($sql) as $post) {
			$id = $post['id'];
			$message = mb_convert_encoding($post['message'], 'Windows-1252', 'utf-8');
			$picture = $post["picture"];
			$fullpicture = $post["fullpicture"];
			$type = $post["type"];
			$object = $post["object"];
			$postid = $post["postid"];
			$link = $post["link"];
			//echo $message." ".$type."<p>";

			if (checkHistory($conn, $campaign, $postid, "identifier")) {
				if (!empty($link)) {
					$result = PostLink($message, $link, $token, $fbpage );
				}
				else{
					$result = PostPhoto($message, $fullpicture, $token, $fbpage );
				}
				print_r($result);
				$createdpost = $result["id"];
				$sql = "UPDATE Viral SET deleted = 'YES' WHERE id = $id ";
				$conn->exec($sql);

				if (!empty($createdpost)) {
					addToLog("Viral", $createdpost, "", $postid, $campaign, $conn);
				}
				else {
					addToLog("Viral", $createdpost, json_encode($result), $postid, $campaign, $conn);
				}
				return;
			}
		}
	}
}

function postFeed($campaign, $conn, $token, $fbpage, $userid, $wp_url) {
	$freq = getFrequency($campaign, $conn, "feedsfreq");

	if (chooseToPost($freq)) {
		$sql = "SELECT * FROM Feeds WHERE campaign = $campaign ORDER BY RAND() ";

		foreach ($conn->query($sql) as $row) {
			$url = $row['URL'];
			// Create lastRSS object
			$rss = new lastRSS;

			// Set cache dir and cache time limit (1200 seconds)
			// (don't forget to chmod cahce dir to 777 to allow writing)
			$rss->cache_dir = '';
			$rss->cache_time = 0;
			$rss->cp = 'UTF-8';
			$rss->date_format = 'l';
			$rss->CDATA = 'strip';

			// Try to load and parse RSS file of Slashdot.org
			$rssurl = $url;
			echo "feed url = ".$rssurl;
			// print_r($rss->get($rssurl));
			if ($rs = $rss->get($rssurl)) {
				foreach ($rs['items'] as $item) {
					$title = $item['title'];
					$feedLink = $item['link'];
					$link = $feedLink;

					if (checkHistory($conn, $campaign, $feedLink, "identifier")) {
						if (!empty($wp_url)) {
							$link = postToWP($wp_url, '', $title, $item['description'], $feedLink, 'url');
						}

						$result = PostLink($title, $link, $token, $fbpage);
						// print_r($result);
						$createdpost = $result["id"];
						addToLog("Feed", $createdpost, "", $feedLink, $campaign, $conn);

						return;
					}
				}
			}
			else {
				echo "Error: It's not possible to get $rssurl";
			}
		}
	}
}

function postCustom($campaign, $conn,$token,$fbpage) {
	$freq = getFrequency($campaign, $conn, "customfreq");

	if (chooseToPost($freq)) {
		$sql = "SELECT * FROM Custom WHERE campaign = $campaign ORDER BY RAND() LIMIT 1 ";

		foreach ($conn->query($sql) as $custom) {
			$campaignID = $custom['campaign'];
			$customurl = $custom['URL'];
			$customid = $custom['id'];
			$custommessage = mb_convert_encoding($custom['message'], 'Windows-1252', 'utf-8');

			$result = PostLink($custommessage, $customurl, $token,$fbpage );
			print_r($result);
			$createdpost = $result["id"];
			addToLog("Promotion",$createdpost , "", $customurl, $campaign, $conn);
			return;
		}
	}
}

function PostPhoto($message, $link, $accesstoken,$fbpage ) {
	$data['message'] = $message;
	$data['url'] = $link ;
	$data['access_token'] = $accesstoken;
	$post_url = 'https://graph.facebook.com/'.$fbpage.'/photos';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $post_url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, TRUE);

	return $result;
}

function PostLink($message, $link, $accesstoken,$fbpage ) {
	$data['link'] = $link;
	$data['message'] = $message;
	$data['access_token'] = $accesstoken;
	$message = html_entity_decode($message);
	$post_url = 'https://graph.facebook.com/'.$fbpage.'/feed';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $post_url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, TRUE);

	return $result;
}

function checkHistory($conn, $campaign, $identifiervalue, $identifiercolumn) {
	$sql = "SELECT * FROM PostLog WHERE $identifiercolumn = '$identifiervalue' AND campaign = $campaign ";
	$res = $conn->query($sql);
	if ($res->fetchColumn() > 0) {
		return 0;
	}
	else {
		return 1;
	}
}

function addToLog($source, $postid, $error, $identifier, $campaign, $conn) {
	$time = time();
	$sql = "INSERT INTO PostLog (source, postid, timesent, error, identifier, campaign)
		VALUES ( '$source', '$postid', '$time', '$error', '$identifier', $campaign )";
	//echo $sql."<p>";
	$conn->exec($sql);
	// print_r($conn->errorInfo());
	return;
}

function getFrequency ($campaign, $conn, $type) {
	$sql = "SELECT $type FROM Campaigns WHERE ID = $campaign ";
	foreach ($conn->query($sql) as $row) {
		$frequency = $row[$type];
		return $frequency;
	}
}

function chooseToPost($frequency) {
	if ($frequency == 0) {
		return 0;
	}

	$random = mt_rand(1,30);
	$powered = pow($frequency, 2);

	if($random <= $powered+1) {
		return 1;
	}
	else {
		return 0;
	}
}

function postToWP($wp_url, $image, $title, $content, $link, $type) {
	if (empty(trim($wp_url))) {
		return false;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $wp_url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array(
		"image"		=> $image,
		"title"		=> $title,
		"content"	=> $content,
		"link"		=> $link,
		"type"		=> $type
	));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($ch);
	curl_close($ch);

	return $result;
}
?>
