<form class="form-horizontal" role="form" method='post' action='getpages.php'>
	<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
	<input type="hidden" name="campaign" value="<?= $campaignID ?>" />

	<div class="form-group">
		<div class="col-lg-10">
			<!-- <button type="submit" class="btn btn-danger" name="test">Fetch Pages</button> -->
			<button type="submit" class="btn btn-danger" name="clear">Clear Pages</button>
		</div>
		<div class="col-lg-2">
			Total Unsupressed: <span id = "selectedpages">
			<?php
				$query = "SELECT count(id) as ct FROM Pages WHERE campaign = ".$campaignID." AND talkingabout >= 100 AND supress2 = 'OFF'";
				$i = 0;
				$res = $db->select($query)[0];
				echo $res[ct];
			?></span>
		</div>
	</div>
</form>

<form class="form-horizontal" role="form" method='post' action='add.php'>
	<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
	<input type="hidden" name="campaign" value="<?= $campaignID ?>" />

	<div class="form-group">
		<label class="col-md-2 control-label">Page Name</label>
		<div class="col-md-3">
			<input type="text" class="form-control" id="pagename" name="name" >
		</div>
		<label class="col-md-2 control-label">Page URL</label>
		<div class="col-md-3">
			<input type="url" class="form-control" id="pageid" name="pageid" >
		</div>
		<div class="col-md-2">
			<button type="submit" class="btn btn-danger" name="addpage">Add Page</button>
		</div>
	</div>
</form>

<div class="adv-table">
	<table class="display table table-bordered table-striped" id="pages-table">
		<thead>
			<tr>
				<th>Name</th>
				<th>ID/URL</th>
				<th>Likes</th>
				<th>Talking About</th>
				<!-- <th>Get Viral</th> -->
				<th>Supress</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$query = "SELECT * FROM Pages WHERE campaign = ".$campaignID." and talkingabout >= 100 ORDER BY supress2 asc";
		$result = $db->select($query);

		foreach ($result as $page) {
			$campaignID = $page['campaign'];
			$id = $page['id'];
			if ( mb_detect_encoding($page['name']) == 'UTF-8' ) {
				$name = $page['name'];
			} else {
				$name = mb_convert_encoding($page['name'], 'Windows-1252', 'utf-8');
			}
			$pageid = $page['pageid'];
			$likes = $page['likes'];
			$talkingabout = $page['talkingabout'];
			$supress = $page['supress']; // this is for viral
			$supress2 = $page['supress2']; // this is for supressing

			echo '<tr class="gradeA">';
			if (preg_match('/^\d+$/', $pageid)) {
				echo '<td><a href="http://facebook.com/'.$pageid.'">'.$name.'</a></td>';
			} else {
				echo '<td><a href="'.$pageid.'">'.$name.'</a></td>';
			}

			echo '<td>'.$pageid.'</td>
					<td>'.$likes.'</td>
					<td>'.$talkingabout.'</td>';
			// if ($supress == "ON") {
			// 	echo '<td><p hidden>A</p><input type="checkbox" checked="" onchange="changeStatus(\''.$id.'\', \''.$campaignID.'\', \'supress\');" /></td>';
			// }
			// else {
			// 	echo '<td><p hidden>B</p><input type="checkbox" onchange="changeStatus(\''.$id.'\', \''.$campaignID.'\', \'supress\');" /></td>';
			// }

			if ($supress2 == "ON") {
				echo' <td><p hidden>A</p><input type="checkbox" checked="" onchange="changeStatus(\''.$id.'\', \''.$campaignID.'\', \'supress2\');" /></td>';
			}
			else {
				echo '<td><p hidden>B</p><input type="checkbox" onchange="changeStatus(\''.$id.'\', \''.$campaignID.'\', \'supress2\');" /></td>';
			}

			echo '<td><a href="?campaign=' . $campaignID . '&view=pages&delete=' . $id . '" style="white-space: nowrap;"><i class="fa fa-trash-o"></i> Delete</a></td>';
			echo '</tr>';
		}
		?>
		<tfoot>
			<tr>
				<th>Name</th>
				<th>ID/URL</th>
				<th>Likes</th>
				<th>Talking About</th>
				<!-- <th>Get Viral</th> -->
				<th>Supress</th>
				<th>Action</th>
			</tr>
		</tfoot>
	</table>
</div>
