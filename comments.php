
	<form class="form-horizontal" role="form" method='post' action='add.php'>
		<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
		<input type="hidden" name="campaign" value="<?= $campaignID ?>" />
  		<input type="hidden" name="slidervalue" value="<?= getSliderValue($campaignID, "commentsfreq"); ?>" id = "commentsfreq" />

		<div class="form-group">
			<label  class="col-lg-2 col-sm-2 control-label">Frequency: </label>
			<div class="col-lg-4">
				<div id="commentsslider" class="slider"></div>
				<div class="slider-info">
					<span id="commentsslider-amount"><?= getSliderValueLabel(getSliderValue($campaignID, "commentsfreq")); ?></span>
				</div>
			</div>
			<div class="col-lg-6">
			</div>
		</div>
		<div class="form-group">
			<label  class="col-lg-2 col-sm-2 control-label">Add New Comment</label>
			<div class="col-lg-7">
				<input type="text" class="form-control" id="comment"  name = "comment" >
			</div>
			<div class="col-lg-3">
				<button  type="button" class="btn btn-danger" name = "test" onclick="testcomment();" >Test</button>
				<button type="submit" class="btn btn-danger" name = "addcomment">Add</button>
			</div>

		</div>
	</form>

	<a href ='spintax.html' target="_blank">Need help with spintax?</a>

	<div class="col-lg-12" id ="testingcomment">
	</div>

	<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="comments-table">
			<thead>
				<tr>
					<th>Comment</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$query = "SELECT * FROM Comments WHERE  campaign =  ".$campaignID." ";
			$result = $db->select($query);
			foreach ($result as $comment){
				$campaignID = $comment['campaign'];
				$commenttxt = mb_convert_encoding($comment['comment'], 'Windows-1252', 'utf-8');
				$commentid = $comment['id'];
				$delete = '<a href = "delete.php?type=Comments&id='.$commentid.'&campaign='.$campaignID.'">Delete</a>';

				echo '<tr class="gradeA">
						<td>'.$commenttxt.'</td>
						<td>'.$delete.'</td>
				</tr>';
			}
			?>
			<tfoot>
				<tr>
					<th>Comment</th>
					<th>Actions</th>
				</tr>
			</tfoot>
		</table>
	</div>
