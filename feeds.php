
	<form class="form-horizontal" role="form" method='post' action='add.php'>
		<input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
		<input type="hidden" name="campaign" value="<?= $campaignID ?>" />
	  	<input type="hidden" name="slidervalue" value="<?= getSliderValue($campaignID, "feedsfreq"); ?>" id = "feedsfreq" />
		<div class="form-group">
			<label  class="col-lg-2 col-sm-2 control-label">Frequency: </label>
			<div class="col-lg-4">
				<div id="slider" class="slider"></div>
				<div class="slider-info">
					<span id="slider-amount"><?= getSliderValueLabel(getSliderValue($campaignID, "feedsfreq")); ?></span>
				</div>
			</div>
			<div class="col-lg-6">
			</div>
		</div>
		<div class="form-group">
			<label  class="col-lg-2 col-sm-2 control-label">Add New URL</label>
			<div class="col-lg-8">
				<input type="text" class="form-control" id="feedurl"  name = "feedurl" >
			</div>
			<div class="col-lg-2">
				<button type="submit" class="btn btn-danger" name = "addfeed">Add</button>
			</div>
		</div>
	</form>

	<div class="adv-table">
		<table  class="display table table-bordered table-striped" id="dynamic-table">
			<thead>
				<tr>
					<th>URL</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$query = "SELECT * FROM Feeds WHERE  campaign =  ".$campaignID." ";
			$result = $db->select($query);

			foreach ($result as $feed){
				$campaignID = $feed['campaign'];
				$url = $feed['URL'];
				$feedid =  $feed['id'];
				$delete = '<a href = "delete.php?type=Feeds&id='.$feedid.'&campaign='.$campaignID.'">Delete</a>';

				echo '<tr class="gradeA">
						<td>'.$url.'</td>
						<td>'.$delete.'</td>
				</tr>';
			}
			?>
			<tfoot>
				<tr>
					<th>URL</th>
					<th>Actions</th>
				</tr>
			</tfoot>
		</table>
	</div>
