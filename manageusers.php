<?php
	include_once dirname(__FILE__) . '/./ASEngine/AS.php';
	include_once dirname(__FILE__) . '/./ASEngine/ASConfig.php';
	include_once dirname(__FILE__) . '/./ASEngine/ASPasswordHasher.php';

	if (! app('login')->isLoggedIn()) {
		redirect("login.php");
	}

	include_once 'profunctions.php';
	$db = getDbConnection();
	$currentUser = app('current_user');

	if ( $currentUser->email != '1337dang@gmail.com' ) {
		echo "
<script>
	window.location.href = '404.html';
</script>";
		exit;
	}

	if ( $_GET['action'] == 'delete' ) {
		$stmt = $db->prepare('DELETE FROM CustomerVerify WHERE id = :id');
		$stmt->execute(array(":id" => $_GET['id']));
		echo json_encode( array('result' => 'success') );
		exit;
	}
	else if ( $_GET['action'] == 'create' ) {
		$stmt = $db->prepare('insert into CustomerVerify values (null, :email, 0)');
		$res = $stmt->execute(array(":email" => trim($_POST['email'])));
		echo json_encode( array('result' => 'success') );
		exit;
	}
	else if ( $_GET['action'] == 'update' ) {
		$stmt = $db->prepare('update CustomerVerify set email=:email where id=:id');
		$stmt->execute(array(":email" => trim($_POST['email']), ":id" => $_GET['id']));
		echo json_encode( array('result' => 'success') );
		exit;
	}
	else if ( $_GET['action'] == 'delete_agency' ) {
		$stmt = $db->prepare('DELETE FROM agencyusers WHERE id = :id');
		$stmt->execute(array(":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_agency') );
		exit;
	}
	else if ( $_GET['action'] == 'create_agency' ) {
		$stmt = $db->prepare('insert into agencyusers values (null, :email)');
		$res = $stmt->execute(array(":email" => trim($_POST['email'])));
		echo json_encode( array('result' => 'success_agency') );
		exit;
	}
	else if ( $_GET['action'] == 'update_agency' ) {
		$stmt = $db->prepare('update agencyusers set email=:email where id=:id');
		$stmt->execute(array(":email" => trim($_POST['email']), ":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_agency') );
		exit;
	}
	else if ( $_GET['action'] == 'delete_oto2' ) {
		$stmt = $db->prepare('DELETE FROM oto2users WHERE id = :id');
		$stmt->execute(array(":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_oto2') );
		exit;
	}
	else if ( $_GET['action'] == 'create_oto2' ) {
		$stmt = $db->prepare('insert into oto2users values (null, :email)');
		$res = $stmt->execute(array(":email" => trim($_POST['email'])));
		echo json_encode( array('result' => 'success_oto2') );
		exit;
	}
	else if ( $_GET['action'] == 'update_oto2' ) {
		$stmt = $db->prepare('update oto2users set email=:email where id=:id');
		$stmt->execute(array(":email" => trim($_POST['email']), ":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_oto2') );
		exit;
	}
	else if ( $_GET['action'] == 'delete_oto1' ) {
		$stmt = $db->prepare('DELETE FROM prousers WHERE id = :id');
		$stmt->execute(array(":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_oto1') );
		exit;
	}
	else if ( $_GET['action'] == 'create_oto1' ) {
		$stmt = $db->prepare('insert into prousers values (null, :email)');
		$res = $stmt->execute(array(":email" => trim($_POST['email'])));
		echo json_encode( array('result' => 'success_oto1') );
		exit;
	}
	else if ( $_GET['action'] == 'update_oto1' ) {
		$stmt = $db->prepare('update prousers set email=:email where id=:id');
		$stmt->execute(array(":email" => trim($_POST['email']), ":id" => $_GET['id']));
		echo json_encode( array('result' => 'success_oto1') );
		exit;
	}
	else if ( $_GET['action'] == 'deleteuser' ) {
		$stmt = $db->prepare('DELETE FROM as_users WHERE user_id = :id');
		$stmt->execute(array(":id" => $_GET['id']));
		$stmt = $db->prepare('DELETE FROM Campaigns WHERE userid = :id');
		$stmt->execute(array(":id" => $_GET['id']));

		echo json_encode( array('result' => 'success') );
		exit;
	}
	else if ( $_GET['action'] == 'updatepassword' ) {
		$hasher = new ASPasswordHasher();
		$stmt = $db->prepare('update as_users set password=:pwd where user_id=:id');
		$stmt->execute(array(
			":pwd" => $hasher->hashPassword(trim($_POST['pwd'])),
			":id" => $_GET['id'])
		);
		echo json_encode( array('result' => 'success_pwdreset') );
		exit;
	}

	include 'templates/header.php';
	$sidebarActive = 'manageusers';
	require 'templates/sidebar.php';

	$users = app('db')->select(
		"SELECT `as_users`.*, `as_user_roles`.`role` as role_name
		FROM `as_users`
		INNER JOIN `as_user_roles` ON `as_users`.`user_role` = `as_user_roles`.`role_id`
		WHERE `as_users`.`user_role` != '3'
		ORDER BY `as_users`.`register_date`"
	);

	$roles = app('db')->select("SELECT * FROM `as_user_roles` WHERE `role_id` != '3'");
?>

<section id="main-content">
	<section class="wrapper">
		<div class="container-fluid">
			<h2>Customer Verify</h2>
			<div class="btn-group">
				<button id="editable-sample_new1" class="btn btn-primary">
					Add New <i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="editable-sample1">
				<thead>
					<tr>
						<th>Email</th>
						<th>Created by</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$pdStmt = $db->prepare("SELECT CustomerVerify.id as id, CustomerVerify.email as cve, as_users.email as aue FROM CustomerVerify left join as_users on CustomerVerify.created_by = as_users.user_id");
					$pdStmt->execute();
					$results = $pdStmt->fetchAll(PDO::FETCH_ASSOC);
					foreach ($results as $result) {
				?><tr data-id="<?= $result["id"] ?>">
						<td><?= $result["cve"] ?></td>
						<td><?= $result["aue"] ?></td>
						<td><a class="edit" href="javascript:;">Edit</a></td>
						<td><a class="delete" href="javascript:;">Delete</a></td>
					</tr><?php } ?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid">
			<h2>Agency Users</h2>
			<div class="btn-group">
				<button id="editable-sample_new2" class="btn btn-primary">
					Add New <i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="editable-sample2">
				<thead>
					<tr>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$pdStmt = $db->prepare("SELECT * FROM agencyusers");
					$pdStmt->execute();
					$results = $pdStmt->fetchAll(PDO::FETCH_ASSOC);
					foreach ($results as $result) {
				?><tr data-id="<?= $result["id"] ?>">
						<td><?= $result["email"] ?></td>
						<td><a class="edit" href="javascript:;">Edit</a></td>
						<td><a class="delete" href="javascript:;">Delete</a></td>
					</tr><?php } ?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid">
			<h2>Oto1 Users</h2>
			<div class="btn-group">
				<button id="editable-sample_new4" class="btn btn-primary">
					Add New <i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="editable-sample4">
				<thead>
					<tr>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$pdStmt = $db->prepare("SELECT * FROM prousers");
					$pdStmt->execute();
					$results = $pdStmt->fetchAll(PDO::FETCH_ASSOC);
					foreach ($results as $result) {
				?><tr data-id="<?= $result["id"] ?>">
						<td><?= $result["email"] ?></td>
						<td><a class="edit" href="javascript:;">Edit</a></td>
						<td><a class="delete" href="javascript:;">Delete</a></td>
					</tr><?php } ?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid">
			<h2>Oto2 Users</h2>
			<div class="btn-group">
				<button id="editable-sample_new3" class="btn btn-primary">
					Add New <i class="fa fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="editable-sample3">
				<thead>
					<tr>
						<th>Email</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$pdStmt = $db->prepare("SELECT * FROM oto2users");
					$pdStmt->execute();
					$results = $pdStmt->fetchAll(PDO::FETCH_ASSOC);
					foreach ($results as $result) {
				?><tr data-id="<?= $result["id"] ?>">
						<td><?= $result["email"] ?></td>
						<td><a class="edit" href="javascript:;">Edit</a></td>
						<td><a class="delete" href="javascript:;">Delete</a></td>
					</tr><?php } ?>
				</tbody>
			</table>
		</div>

		<div class="container-fluid">
			<h2>Users</h2>
		</div>
		<div class="table-responsive container-fluid">
			<table class="table table-striped table-hover table-bordered" id="users-table">
				<thead>
					<tr>
						<th><?= trans('username') ?></th>
						<th><?= trans('email') ?></th>
						<th><?= trans('register_date') ?></th>
						<th><?= trans('confirmed') ?></th>
						<th>Domain</th>
						<th>Banned?</th>
						<th><?= trans('action') ?></th>
						<th><?= trans('action') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user) : ?><tr class="user-row">
						<td><?= e($user['username']) ?></td>
						<td><?= e($user['email']) ?></td>
						<td><?= $user['register_date'] ?></td>
						<td><?php echo $user['confirmed'] == "Y"
								? "<p class='text-success'>" . trans('yes') . "</p>"
								: "<p class='text-error'>" . trans('no') . "</p>"
							?></td>
						<td><?= $user['domain'] ?></td>
						<td><?php echo $user['banned'] == "Y"
								? "<p class='text-success'>" . trans('yes') . "</p>"
								: "<p class='text-error'>" . trans('no') . "</p>"
							?></td>
						<td style='white-space: nowrap;'>
							<a href="javascript:;" class="user-password" title="Edit User" data-id="<?= $user["user_id"] ?>">
								Reset Password</a>
						</td>
						<td style='white-space: nowrap;'>
							<a href="javascript:;" class="delete-user" title="Delete User" data-id="<?= $user["user_id"] ?>"><i class="fa fa-trash-o"></i> Delete</a>

						</td>
					</tr><?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</section>
</section>
<div class="modal fade" id="password-reset" tabindex="-1" role="dialog" aria-labelledby="password-reset">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="password-reset">Reset Password</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal password-reset-form" role="form">
					<input type="hidden" class="form-control" id="userid" name="userid">
					<div class="form-group">
						<label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Password</label>
						<div class="col-lg-10">
							<input type="password" class="form-control" id="password1" name='password1'>
						</div>
					</div>
					<div class="form-group">
						<label for="inputPassword2" class="col-lg-2 col-sm-2 control-label">Confirm Password</label>
						<div class="col-lg-10">
							<input type="password" class="form-control" id="password2" name='password2'>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-save">Save changes</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="assets/js/sha512.js"></script>
<script type="text/javascript">
var EditableTable1 = function () {
	return {
		init: function () {
			function restoreRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);

				for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
					oTable.fnUpdate(aData[i], nRow, i, false);
				}

				oTable.fnDraw();
			}

			function editRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);
				jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
				jqTds[2].innerHTML = '<a class="edit" href="">Save</a>';
				jqTds[3].innerHTML = '<a class="cancel" href="">Cancel</a>';
			}

			function saveRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 2, false);
				oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 3, false);
				oTable.fnDraw();
			}

			function cancelEditRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 2, false);
				oTable.fnDraw();
			}

			var oTable = $('#editable-sample1').dataTable({
				"aLengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"] // change per page values here
				],
				// set the initial value
				"iDisplayLength": 10,
				"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"oPaginate": {
						"sPrevious": "Prev",
						"sNext": "Next"
					}
				},
				"aoColumnDefs": [{
						'bSortable': true,
						'aTargets': [0]
					}
				]
			});

			// jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall");

			var nEditing = null;

			$('#editable-sample_new1').click(function (e) {
				e.preventDefault();

				var aiNew = oTable.fnAddData(['', '',
					'<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
				]);
				var nRow = oTable.fnGetNodes(aiNew[0]);
				editRow(oTable, nRow);
				nEditing = nRow;
			});

			$(document).on('click', '#editable-sample1 a.delete', function(e) {
				e.preventDefault();

				if (confirm("Are you sure to delete this row?") == false) {
					return;
				}

				var nRow = $(this).parents('tr')[0];

				$.ajax({
					url: "?action=delete&id=" + $(nRow).data('id'),
					type: "GET",
					success: function (result) {
						var msg = 'Email deleted successfully.';
						toastr.options.fadeOut = 3000;
						toastr.success(msg);

						oTable.fnDeleteRow(nRow);
					}
				}).fail( function() {
					var msg = 'Error occured and cannot delete. Please try again.';
					toastr.options.fadeOut = 3000;
					toastr.error(msg);
				});
			});

			$(document).on('click', '#editable-sample1 a.cancel', function(e) {
				e.preventDefault();
				if ($(this).attr("data-mode") == "new") {
					var nRow = $(this).parents('tr')[0];
					oTable.fnDeleteRow(nRow);
				} else {
					restoreRow(oTable, nEditing);
					nEditing = null;
				}
			});

			$(document).on('click', '#editable-sample1 a.edit', function(e) {
				e.preventDefault();
				var nRow = $(this).parents('tr')[0];
				if (nEditing !== null && nEditing != nRow) {
					restoreRow(oTable, nEditing);
					editRow(oTable, nRow);
					nEditing = nRow;
				} else if (nEditing == nRow && this.innerHTML == "Save") {
					saveRow(oTable, nEditing);
					nEditing = null;

					var url = "";
					var type = 'create';

					if ( $(nRow).data('id') ) {
						url = "?action=update&id=" + $(nRow).data('id');
						type = 'update';
					}
					else {
						url = "?action=create";
					}

					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: {
							email: $(nRow).find('td:first-child').html(),
							"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>"
						},
						success: function (result) {
							var msg = 'Email added successfully.';
							if (type == 'update') msg = 'Email updated successfully.'
							toastr.options.fadeOut = 3000;
							toastr.success(msg);
						}
					}).fail( function() {
						var msg = 'Error occured and can not save data. Please try again.';
						toastr.options.fadeOut = 3000;
						toastr.error(msg);
					});
				} else {
					editRow(oTable, nRow);
					nEditing = nRow;
				}
			});
		}
	};
}();

var EditableTable2 = function () {
	return {
		init: function () {
			function restoreRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);

				for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
					oTable.fnUpdate(aData[i], nRow, i, false);
				}

				oTable.fnDraw();
			}

			function editRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);
				jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<a class="edit" href="">Save</a>';
				jqTds[2].innerHTML = '<a class="cancel" href="">Cancel</a>';
			}

			function saveRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
				oTable.fnDraw();
			}

			function cancelEditRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnDraw();
			}

			var oTable = $('#editable-sample2').dataTable({
				"aLengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"] // change per page values here
				],
				// set the initial value
				"iDisplayLength": 10,
				"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"oPaginate": {
						"sPrevious": "Prev",
						"sNext": "Next"
					}
				},
				"aoColumnDefs": [{
						'bSortable': true,
						'aTargets': [0]
					}
				]
			});

			// jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall");

			var nEditing = null;

			$('#editable-sample_new2').click(function (e) {
				e.preventDefault();

				var aiNew = oTable.fnAddData(['', '',
					'<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
				]);
				var nRow = oTable.fnGetNodes(aiNew[0]);
				editRow(oTable, nRow);
				nEditing = nRow;
			});

			$(document).on('click', '#editable-sample2 a.delete', function(e) {
				e.preventDefault();

				if (confirm("Are you sure to delete this row?") == false) {
					return;
				}

				var nRow = $(this).parents('tr')[0];

				$.ajax({
					url: "?action=delete_agency&id=" + $(nRow).data('id'),
					type: "GET",
					success: function (result) {
						var msg = 'Email deleted successfully.';
						toastr.options.fadeOut = 3000;
						toastr.success(msg);

						oTable.fnDeleteRow(nRow);
					}
				}).fail( function() {
					var msg = 'Error occured and cannot delete. Please try again.';
					toastr.options.fadeOut = 3000;
					toastr.error(msg);
				});
			});

			$(document).on('click', '#editable-sample2 a.cancel', function(e) {
				e.preventDefault();
				if ($(this).attr("data-mode") == "new") {
					var nRow = $(this).parents('tr')[0];
					oTable.fnDeleteRow(nRow);
				} else {
					restoreRow(oTable, nEditing);
					nEditing = null;
				}
			});

			$(document).on('click', '#editable-sample2 a.edit', function(e) {
				e.preventDefault();
				var nRow = $(this).parents('tr')[0];
				if (nEditing !== null && nEditing != nRow) {
					restoreRow(oTable, nEditing);
					editRow(oTable, nRow);
					nEditing = nRow;
				} else if (nEditing == nRow && this.innerHTML == "Save") {
					saveRow(oTable, nEditing);
					nEditing = null;

					var url = "";
					var type = 'create';

					if ( $(nRow).data('id') ) {
						url = "?action=update_agency&id=" + $(nRow).data('id');
						type = 'update';
					}
					else {
						url = "?action=create_agency";
					}

					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: {
							email: $(nRow).find('td:first-child').html(),
							"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>"
						},
						success: function (result) {
							var msg = 'Email added successfully.';
							if (type == 'update') msg = 'Email updated successfully.'
							toastr.options.fadeOut = 3000;
							toastr.success(msg);
						}
					}).fail( function() {
						var msg = 'Error occured and can not save data. Please try again.';
						toastr.options.fadeOut = 3000;
						toastr.error(msg);
					});
				} else {
					editRow(oTable, nRow);
					nEditing = nRow;
				}
			});
		}
	};
}();

var EditableTable3 = function () {
	return {
		init: function () {
			function restoreRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);

				for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
					oTable.fnUpdate(aData[i], nRow, i, false);
				}

				oTable.fnDraw();
			}

			function editRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);
				jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<a class="edit" href="">Save</a>';
				jqTds[2].innerHTML = '<a class="cancel" href="">Cancel</a>';
			}

			function saveRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
				oTable.fnDraw();
			}

			function cancelEditRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnDraw();
			}

			var oTable = $('#editable-sample3').dataTable({
				"aLengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"] // change per page values here
				],
				// set the initial value
				"iDisplayLength": 10,
				"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"oPaginate": {
						"sPrevious": "Prev",
						"sNext": "Next"
					}
				},
				"aoColumnDefs": [{
						'bSortable': true,
						'aTargets': [0]
					}
				]
			});

			// jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall");

			var nEditing = null;

			$('#editable-sample_new3').click(function (e) {
				e.preventDefault();

				var aiNew = oTable.fnAddData(['', '',
					'<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
				]);
				var nRow = oTable.fnGetNodes(aiNew[0]);
				editRow(oTable, nRow);
				nEditing = nRow;
			});

			$(document).on('click', '#editable-sample3 a.delete', function(e) {
				e.preventDefault();

				if (confirm("Are you sure to delete this row?") == false) {
					return;
				}

				var nRow = $(this).parents('tr')[0];

				$.ajax({
					url: "?action=delete_oto2&id=" + $(nRow).data('id'),
					type: "GET",
					success: function (result) {
						var msg = 'Email deleted successfully.';
						toastr.options.fadeOut = 3000;
						toastr.success(msg);

						oTable.fnDeleteRow(nRow);
					}
				}).fail( function() {
					var msg = 'Error occured and cannot delete. Please try again.';
					toastr.options.fadeOut = 3000;
					toastr.error(msg);
				});
			});

			$(document).on('click', '#editable-sample3 a.cancel', function(e) {
				e.preventDefault();
				if ($(this).attr("data-mode") == "new") {
					var nRow = $(this).parents('tr')[0];
					oTable.fnDeleteRow(nRow);
				} else {
					restoreRow(oTable, nEditing);
					nEditing = null;
				}
			});

			$(document).on('click', '#editable-sample3 a.edit', function(e) {
				e.preventDefault();
				var nRow = $(this).parents('tr')[0];
				if (nEditing !== null && nEditing != nRow) {
					restoreRow(oTable, nEditing);
					editRow(oTable, nRow);
					nEditing = nRow;
				} else if (nEditing == nRow && this.innerHTML == "Save") {
					saveRow(oTable, nEditing);
					nEditing = null;

					var url = "";
					var type = 'create';

					if ( $(nRow).data('id') ) {
						url = "?action=update_oto2&id=" + $(nRow).data('id');
						type = 'update';
					}
					else {
						url = "?action=create_oto2";
					}

					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: {
							email: $(nRow).find('td:first-child').html(),
							"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>"
						},
						success: function (result) {
							var msg = 'Email added successfully.';
							if (type == 'update') msg = 'Email updated successfully.'
							toastr.options.fadeOut = 3000;
							toastr.success(msg);
						}
					}).fail( function() {
						var msg = 'Error occured and can not save data. Please try again.';
						toastr.options.fadeOut = 3000;
						toastr.error(msg);
					});
				} else {
					editRow(oTable, nRow);
					nEditing = nRow;
				}
			});
		}
	};
}();

var EditableTable4 = function () {
	return {
		init: function () {
			function restoreRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);

				for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
					oTable.fnUpdate(aData[i], nRow, i, false);
				}

				oTable.fnDraw();
			}

			function editRow(oTable, nRow) {
				var aData = oTable.fnGetData(nRow);
				var jqTds = $('>td', nRow);
				jqTds[0].innerHTML = '<input type="text" class="form-control small" value="' + aData[0] + '">';
				jqTds[1].innerHTML = '<a class="edit" href="">Save</a>';
				jqTds[2].innerHTML = '<a class="cancel" href="">Cancel</a>';
			}

			function saveRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 2, false);
				oTable.fnDraw();
			}

			function cancelEditRow(oTable, nRow) {
				var jqInputs = $('input', nRow);
				oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
				oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 1, false);
				oTable.fnDraw();
			}

			var oTable = $('#editable-sample4').dataTable({
				"aLengthMenu": [
					[10, 25, 50, -1],
					[10, 25, 50, "All"] // change per page values here
				],
				// set the initial value
				"iDisplayLength": 10,
				"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"oPaginate": {
						"sPrevious": "Prev",
						"sNext": "Next"
					}
				},
				"aoColumnDefs": [{
						'bSortable': true,
						'aTargets': [0]
					}
				]
			});

			// jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall");

			var nEditing = null;

			$('#editable-sample_new4').click(function (e) {
				e.preventDefault();

				var aiNew = oTable.fnAddData(['', '',
					'<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
				]);
				var nRow = oTable.fnGetNodes(aiNew[0]);
				editRow(oTable, nRow);
				nEditing = nRow;
			});

			$(document).on('click', '#editable-sample4 a.delete', function(e) {
				e.preventDefault();

				if (confirm("Are you sure to delete this row?") == false) {
					return;
				}

				var nRow = $(this).parents('tr')[0];

				$.ajax({
					url: "?action=delete_oto1&id=" + $(nRow).data('id'),
					type: "GET",
					success: function (result) {
						var msg = 'Email deleted successfully.';
						toastr.options.fadeOut = 3000;
						toastr.success(msg);
						oTable.fnDeleteRow(nRow);
					}
				}).fail( function() {
					var msg = 'Error occured and cannot delete. Please try again.';
					toastr.options.fadeOut = 3000;
					toastr.error(msg);
				});
			});

			$(document).on('click', '#editable-sample4 a.cancel', function(e) {
				e.preventDefault();
				if ($(this).attr("data-mode") == "new") {
					var nRow = $(this).parents('tr')[0];
					oTable.fnDeleteRow(nRow);
				} else {
					restoreRow(oTable, nEditing);
					nEditing = null;
				}
			});

			$(document).on('click', '#editable-sample4 a.edit', function(e) {
				e.preventDefault();
				var nRow = $(this).parents('tr')[0];
				if (nEditing !== null && nEditing != nRow) {
					restoreRow(oTable, nEditing);
					editRow(oTable, nRow);
					nEditing = nRow;
				} else if (nEditing == nRow && this.innerHTML == "Save") {
					saveRow(oTable, nEditing);
					nEditing = null;

					var url = "";
					var type = 'create';

					if ( $(nRow).data('id') ) {
						url = "?action=update_oto1&id=" + $(nRow).data('id');
						type = 'update';
					}
					else {
						url = "?action=create_oto1";
					}

					$.ajax({
						url: url,
						type: "POST",
						dataType: "json",
						data: {
							email: $(nRow).find('td:first-child').html(),
							"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>"
						},
						success: function (result) {
							var msg = 'Email added successfully.';
							if (type == 'update') msg = 'Email updated successfully.'
							toastr.options.fadeOut = 3000;
							toastr.success(msg);
						}
					}).fail( function() {
						var msg = 'Error occured and can not save data. Please try again.';
						toastr.options.fadeOut = 3000;
						toastr.error(msg);
					});
				} else {
					editRow(oTable, nRow);
					nEditing = nRow;
				}
			});
		}
	};
}();

jQuery(document).ready(function($) {
	EditableTable1.init();
	EditableTable2.init();
	EditableTable3.init();
	EditableTable4.init();

	var dTable = $('#users-table').dataTable( {
	} );

	$(document).on('click', '#users-table a.user-password', function(e) {
		e.preventDefault();
		$('#password-reset').modal({ show: true });
		$("#userid").val($(this).data('id'));
	});

	$(document).on('click', ".btn-save", function(e) {
		var pwd1 = $("#password1");
		var pwd2 = $("#password2");

		if (pwd1.val().length === 0) {
			pwd1.focus(); return;
		} else if (pwd2.val().length === 0) {
			pwd2.focus(); return;
		} else if (pwd1.val() != pwd2.val()) {
			pwd1.focus(); alert('Passwords does not match'); return;
		}

		$.ajax({
			url: "?action=updatepassword&id=" + $("#userid").val(),
			type: "POST",
			data: {
				"<?= ASCsrf::getTokenName() ?>": "<?= ASCsrf::getToken() ?>",
				pwd: CryptoJS.SHA512(pwd1.val()).toString()
			},
			success: function (result) {
				var msg = 'Password reset successfully.';
				toastr.options.fadeOut = 3000;
				toastr.success(msg);

				$('#password-reset').modal({ show: false });
			}
		}).fail( function() {
			var msg = 'Error occured and cannot delete. Please try again.';
			alert(msg);
		});

	});

	$(document).on('click', '#users-table a.delete-user', function(e) {
		e.preventDefault();

		if (confirm("Are you sure to delete this row?") == false) {
			return;
		}

		var nRow = $(this).parents('tr')[0];
		dTable.fnDeleteRow(nRow);

		$.ajax({
			url: "?action=deleteuser&id=" + $(this).data('id'),
			type: "GET",
			success: function (result) {
				var msg = 'User deleted successfully.';
				toastr.options.fadeOut = 3000;
				toastr.success(msg);
			}
		}).fail( function() {
			var msg = 'Error occured and cannot delete. Please try again.';
			toastr.options.fadeOut = 3000;
			toastr.error(msg);
		});
	});
});

</script>

<?php include 'templates/footer.php'; ?>

