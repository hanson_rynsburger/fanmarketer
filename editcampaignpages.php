<?php
$campaignID = $_GET["campaign"];
$editpage = "editcampaign.php?campaign=".$campaignID;
$view = $_GET['view'];
if ($view === NULL or $view == '') $view = 'setup';
?>
<div class="tab-pane <?= ($view == 'setup') ? 'active' : '' ?>" id="setup">
	<? if ($view == 'setup') include 'setup.php' ?>
</div>
<div class="tab-pane <?= ($view == 'pages') ? 'active' : '' ?>" id="pages">
	<? if ($view == 'pages') include 'pages.php' ?>
</div>
<div class="tab-pane <?= ($view == 'viral') ? 'active' : '' ?>" id="viral">
	<? if ($view == 'viral') include 'viral.php' ?>
</div>
<div class="tab-pane <?= ($view == 'feeds') ? 'active' : '' ?>" id="feeds">
	<? if ($view == 'feeds') include 'feeds.php' ?>
</div>
<div class="tab-pane <?= ($view == 'video') ? 'active' : '' ?>" id="video">
	<? if ($view == 'video') include 'video.php' ?>
</div>
<? if ($oto1 == "YES") { ?>
<div class="tab-pane <?= ($view == 'images') ? 'active' : '' ?>" id="images">
	<? if ($view == 'images') include 'images.php' ?>
</div>
<? } ?>
<? if ($oto1 == "YES") { ?>
<div class="tab-pane <?= ($view == 'custom') ? 'active' : '' ?>" id="custom">
	<? if ($view == 'custom') include 'custom.php'; ?>
</div>
<? } ?>
<div class="tab-pane <?= ($view == 'comments') ? 'active' : '' ?>" id="comments">
  	<? if ($view == 'comments') include 'comments.php' ?>
</div>
<div class="tab-pane <?= ($view == 'replies') ? 'active' : '' ?>" id="replies">
	<? if ($view == 'replies') include 'replies.php' ?>
</div>
<? if ($oto2 == "YES") { ?>
<div class="tab-pane <?= ($view == 'list') ? 'active' : '' ?>" id="list">
	<? if ($view == 'list') include 'list.php'; ?>
</div>
<? } ?>
<? if ($oto1 == "YES") { ?>
<div class="tab-pane <?= ($view == 'growth') ? 'active' : '' ?>" id="growth">
	<? if ($view == 'growth') include 'growth.php'; ?>
</div>
<? } ?>
<div class="tab-pane <?= ($view == 'log') ? 'active' : '' ?>" id="log">
	<? if ($view == 'log') include 'postlog.php' ?>
</div>
