<!--sidebar start-->
<aside>
	<div id="sidebar"  class="nav-collapse ">
		<ul class="sidebar-menu">
			<li>
			  <a href="index.php" <?php if ($sidebarActive == "home") { echo "class = 'active'"; } ?> >
				  <i class="fa fa-dashboard"></i>
				  <span>Dashboard</span>
			  </a>
			</li>
			<li class="sub-menu">
				<a href="settings.php" <?php if ($sidebarActive == "settings") { echo "class = 'active'"; } ?> >
					<i class="fa fa-cogs"></i>
					<span>Settings</span>
					<span class="arrow"></span>
				</a>
			</li>
			<li class="sub-menu">
				<a href="campaigns.php" <?php if ($sidebarActive == "campaigns") { echo "class = 'active'"; } ?> >
					<i class="fa fa-rocket"></i>
					<span>Campaigns</span>
					<span class="arrow"></span>
				</a>
			</li>
			<?php
			require_once ('profunctions.php');
			if ($currentUser->email == '1337dang@gmail.com') { ?>
			<li class="sub-menu">
				<a href="manageusers.php" <?php if ($sidebarActive == "manageusers") { echo "class = 'active'"; } ?> >
					<i class="fa fa-fire"></i>
					<span>Manage Users</span>
					<span class="arrow"></span>
				</a>
			</li>
			<li class="sub-menu">
				<a href="messages.php" <?php if ($sidebarActive == "messages") { echo "class = 'active'"; } ?> >
					<i class="fa fa-microphone"></i>
					<span>Messages</span>
					<span class="arrow"></span>
				</a>
			</li>
			<?php } else if ( isAgencyUser($currentUser->id) ) { ?>
			<li class="sub-menu">
				<a href="agency.php" <?php if ($sidebarActive == "agency") { echo "class = 'active'"; } ?> >
					<i class="fa fa-fire"></i>
					<span>Agency</span>
					<span class="arrow"></span>
				</a>
			</li>		
			<?php }
			else if ( isAgency10($currentUser->id) ) { ?>
			<li class="sub-menu">
				<a href="agency10.php" <?php if ($sidebarActive == "agency") { echo "class = 'active'"; } ?> >
					<i class="fa fa-fire"></i>
					<span>Agency</span>
					<span class="arrow"></span>
				</a>
			</li>		
			<?php }
			else if ( isAgency20($currentUser->id) ) { ?>
			<li class="sub-menu">
				<a href="agency20.php" <?php if ($sidebarActive == "agency") { echo "class = 'active'"; } ?> >
					<i class="fa fa-fire"></i>
					<span>Agency</span>
					<span class="arrow"></span>
				</a>
			</li>		
			<?php }
			else if ( isAgency100($currentUser->id) ) { ?>
			<li class="sub-menu">
				<a href="agency100.php" <?php if ($sidebarActive == "agency") { echo "class = 'active'"; } ?> >
					<i class="fa fa-fire"></i>
					<span>Agency</span>
					<span class="arrow"></span>
				</a>
			</li>		
			<?php } ?>
			<li class="sub-menu">
				<a href="tutorials.php" class="">
					<i class="fa fa-book"></i>
					<span>Tutorials</span>
					<span class="arrow"></span>
				</a>
			</li>
			<li class="sub-menu">
				<a href="http://bytemarketer.zendesk.com/" class="">
					<i class="fa fa-envelope"></i>
					<span>Support</span>
					<span class="arrow"></span>
				</a>
			</li>
		</ul>
		<!-- sidebar menu end-->
	</div>
</aside>
<!--sidebar end-->
