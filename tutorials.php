<?php
	include 'templates/header.php';
	// Include sidebar template
	// and set active page to "home".
	$sidebarActive = 'home';
	require 'templates/sidebar.php';
?>

<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<div class="row"><div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					Tutorials
				</header>
				<div class="panel-body">
					<h3><strong>#1 Facebook app setup</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/fqu_pdaD4PY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#2 Creating a campaign</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/xX0ffA55H8Y?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#3 Pages</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/QiPJywjzH6k?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#4 Viral</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/4SSg1Bgb1Js?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#5 RSS Feeds</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/fCKtJsTKsfQ?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#6 Youtube Videos</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/jfBlsIOmKRM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#7 Comments</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/udYC4_WjQkU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#8 Replies</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/BbZeQIpD-Vc?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#9 PRO Version</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/t13c4kk2Iuk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#10 List Builder</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/M-Z8pENVirk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
					<h3><strong>#11 Agency</strong></h3>
					<p><iframe width="853" height="480" src="https://www.youtube.com/embed/bOh9ALmcxTE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
				</div>
			</section>
		</div></div>
	</section>
</section>

<!--main content end-->

<?php include 'templates/footer.php'; ?>

<script src="ASLibrary/js/index.js"></script>

</body>
</html>
