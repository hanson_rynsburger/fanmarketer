<?php
	include_once dirname(__FILE__) . '/./ASEngine/AS.php';

	if (! app('login')->isLoggedIn()) {
		return '';
	}

	$currentUser = app('current_user');
	if ( $currentUser->email != '1337dang@gmail.com' ) {
		return '';
	}

	$users = app('db')->select(
		"SELECT `as_users`.*, `as_user_roles`.`role` as role_name
		FROM `as_users`
		INNER JOIN `as_user_roles` ON `as_users`.`user_role` = `as_user_roles`.`role_id`
		WHERE `as_users`.`user_role` != '3'
		ORDER BY `as_users`.`register_date` DESC"
	);

	$totalCount = 0;
	$data = array();

	foreach ($users as $user) {
		array_push($data, array(
			e($user['username']),
			e($user['email']),
			$user['register_date'],
			$user['confirmed'] == "Y" ? trans('yes') : trans('no'),
			$user['domain'],
			$user['banned'] == "Y" ? trans('yes') : trans('no')
		))
		$totalCount ++;
	}

	$out = array(
		"draw" => intval($_POST['draw']),
		"recordTotal" => $totalCount,
		"recordsFiltered" => $totalCount,
		"data" => $data
	)

	echo json_encode($out);