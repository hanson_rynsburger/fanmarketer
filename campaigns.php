<?php
	include 'templates/header.php';
	include_once 'profunctions.php';

	$db = app('db');
	$userid = ASSession::get('user_id');

	if(isset($_GET['delete'])) {
		$id = $_GET['delete'];
		$db->delete(
			"Campaigns",
			"ID = :id",
			array("id" => $id)
		);
	}

	$campaignCountsActive = getCampaignCounts($userid);
	$isOto1 = gotOto1($userid);

	if(isset($_POST['addnew'])) {
		if ( !$isOto1 && $campaignCountsActive >= 5 ) {
			echo "<script>
				alert('Please upgrade your plan');
			</script>";
		} else {
			$db->insert('Campaigns', array(
				"name" => "",
				"userid" => $userid,
				"viralstatus" => "ON"
			));

			$id = $db->lastInsertId();

			$_GET['edit'] = $id;

			echo "<script>
				window.location.replace('editcampaign.php?campaign=".$id."');
			</script>";
		}
	}
?>

<div>
    <?php
        // Include sidebar template
        // and set active page to "home".
        $sidebarActive = 'campaigns';
        require 'templates/sidebar.php';
    ?>

<!--main content start-->

<script>
function changeStatus(id) {
	request = jQuery.ajax({
			type: 'post',
			  url: 'setstatus.php',
			  data: { id: id}
			  })
	.done(function( msg ) {
	});
}
</script>
<section id="main-content">
	<section class="wrapper">

	<!-- page start-->
	<div class="row">
		<div class="col-sm-12">
			<section class="panel">
				<form  class="form-horizontal tasi-form" role="form" method='post' action=''>
				   <input type="hidden" name="<?= ASCsrf::getTokenName() ?>" value="<?= ASCsrf::getToken() ?>" />
					<header class="panel-heading">
						  Campaigns
					</header>

					<div class="panel-body">
						<? if ( !$isOto1 && $campaignCountsActive >= 5 ) { ?>
						<div class="alert alert-warning" role="alert">
							You can create a maximum of 5 campaigns. To create UNLIMITED campaigns please <a href ="http://fanmarketer.com/sales/oto1.html">upgrade to the pro version</a>
						</div>
						<? } else { ?>
						<button  type="submit" class="btn btn-danger" name = "addnew">Add New Campaign</button>
						<? } ?>
						<div class="adv-table">
							<table  class="display table table-bordered table-striped" id="dynamic-table">
								<thead>
									<tr>
										<th>Name</th>
										<th>Actions</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$query = "SELECT * FROM Campaigns WHERE name != '' AND userid =  ".$userid." ";

								$result = $db->select($query);

								foreach ($result as $campaign) {
									$campaignID = $campaign['ID'];
									$name = $campaign['name'];
									$status =  $campaign['status'];
									$editpage = "editcampaign.php?campaign=".$campaignID;
									$log = "editcampaign.php?campaign=" . $campaignID . "&view=log";
									$deletepage = "campaigns.php?delete=" . $campaignID;

									echo '<tr class="gradeA">
											<td>'.$name.'</td>
											<td><a href ="'.$editpage.'">Edit</a>|<a href="'.$deletepage.'" onclick="if (!confirm(\'Are you sure?\')) return false;">Delete</a>| <a href = "'.$log.'" >Log</a></td>';

											if ($status == "ON") {
												echo' <td><input type="checkbox" checked="" data-toggle="switch"  onchange = "changeStatus(\''.$campaignID.'\');" /></td>';
											}
											else{
												echo '<td><input type="checkbox" data-toggle="switch"  onchange = "changeStatus(\''.$campaignID.'\');" /></td>';
											}
										echo'</tr>';
								}


								?>


								<tfoot>
									<tr>
										<th>Name</th>
										<th>Actions</th>
										<th>Status</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
	</section>
</section>
<!--main content end-->
<?php include 'templates/footer.php'; ?>
<script src="ASLibrary/js/index.js"></script>
</body>
</html>
