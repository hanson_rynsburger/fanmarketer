<?php

include 'templates/header.php';
include 'facebookFunctions.php';
include 'profunctions.php';


$db = app('db');
$userid = ASSession::get('user_id');
$oto1 = gotOto1($userid);
$oto2 = gotOto2($userid);


$campaignID = $_GET["campaign"];



$result = $db->select(
    "SELECT * FROM `settings` WHERE `userid` = :id",
    array ("id" => $userid)
);
foreach($result as $user) {    
   $fbid = $user['fbid'];  
   $secret = $user['fbsecret'];
   $google =  $user['googleid'];
   $timezone = $user['timezone'];
   $token = $user['fbaccesstoken'];

}
$result = $db->select(
    "SELECT * FROM `Campaigns` WHERE  `ID` = :campaign  ",
    array ("campaign"=>$campaignID)
);


//print_r($result);

foreach($result as $campaign) {    
   $name = $campaign['name'];  
   $keyword1 = $campaign['keyword1'];
   $keyword2 =  $campaign['keyword2'];
   $keyword3 = $campaign['keyword3'];  
    $fbpage = $campaign['pageid'];   
    $viralApproval = $campaign['viralstatus'];   
}

if(isset($_POST['save'])){


	$keyword1 = trim($_POST['keyword1']);
	$keyword2 =  trim($_POST['keyword2']);
	$keyword3 =  trim($_POST['keyword3']);		
	$name =  trim($_POST['name']);	
	$fbpage =  trim($_POST['fbpage']);	
	
	
	if (!empty($name)){
			//echo "<p><p><p><p><p>Campaign =".$campaign."<p>Name =".$name;	
				
			$db->update('Campaigns',
			array ("name" => $name, "keyword1" => $keyword1, "keyword2" =>$keyword2, "keyword3" => $keyword3, "userid" => $userid, "pageid" =>$fbpage  ),
			"ID = :campaign",
			array("campaign" => $campaignID)
			);
	}
	else{
		$db->insert('Campaigns', array(
			"name" => " ",
			"keyword1"  => $keyword1,
			"keyword2"  => $keyword2,
			"keyword3" => $keyword3,
			"userid" => $userid,
			"pageid" =>$fbpage,
			"feedsfreq" => 3,  
			"videosfreq" => 3,  
			"commentsfreq" => 3,  
			"repliesfreq" => 3 
		));
	}	
	
	

}

function getSliderValue($campaign, $type){

	$db = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');
	

	$sql= "SELECT ".$type." FROM `Campaigns` WHERE  `ID` = ".$campaign." ";
	$stmt = $db->query($sql); 
	$row =$stmt->fetchObject();

	
	return $row ->$type;
		
}	
function getSliderValueLabel($value){

	
	if ($value == 0 ){
		return "Never";
	}	
	else if ($value == 1 ){
		return "Much Less Often";
	}	
	else if ($value == 2 ){
		return "Less Often";
	}	
	else if ($value == 3 ){
		return "Standard";
	}	
	else if ($value == 4 ){
		return "More Often";
	}	
	else if ($value == 5 ){
		return "Much More Often";
	}	
	
	else {	
		return "Standard";
	}	
}	





?>
        
<div class="row">
    <?php
        // Include sidebar template
        // and set active page to "home".
        $sidebarActive = 'home';
	
        require 'templates/sidebar.php';
    ?>

<!--main content start-->
<script>
	
	function pagesWarning(){
		request = jQuery.ajax({
		type: 'post',
		url: 'countselected.php',
		data: { campaign: '<?php echo $campaignID  ?>'}
		})
		.done(function( msg ) {	
			if (msg < 50){
				var warning = 'You currently have '+msg+' pages selected. Please select at least 50 for the campaign to start';
				
				toastr.options.fadeOut = 3000;
				toastr.warning(warning);
			}	
		});		  
	}
	
	function commentsWarning(){
		request = jQuery.ajax({
		type: 'post',
		url: 'countComments.php',
		data: { campaign: '<?php echo $campaignID  ?>'}
		})
		.done(function( msg ) {	
		
			if (msg < 15){
				var warning = 'Please create at least 15 comments. The software will LIKE only until you have at created at least 15 comments';
			
				toastr.options.fadeOut = 3000;
				toastr.warning(warning);
			}	
		});		  
	}
	
	function repliesWarning(){
		request = jQuery.ajax({
		type: 'post',
		url: 'countReplies.php',
		data: { campaign: '<?php echo $campaignID  ?>'}
		})
		.done(function( msg ) {	
		
			if (msg < 15){
				var warning = 'Please create at least 15 different replies. The software will not do replies until these have been created';
				
				toastr.options.fadeOut = 3000;
				toastr.warning(warning);
			}	
		});		  
	}
	
	
	function changeStatus(pageid, campaign){
	
		request = jQuery.ajax({
				type: 'post',
				  url: 'setpagestatus.php',
				  data: { pageid: pageid, campaign: campaign}
				  })
				    .done(function( msg ) {
					
				  });
				  
				  
		request = jQuery.ajax({
				type: 'post',
				  url: 'countselected.php',
				  data: { pageid: pageid, campaign: campaign}
				  })
				    .done(function( msg ) {				
					 $("#selectedpages").html(msg);
				  });		  
	}
	
	function changeViralStatus(campaign){
	
		request = jQuery.ajax({
				type: 'post',
				  url: 'setviralstatus.php',
				  data: { campaign: campaign}
				  })
				    .done(function( msg ) {
					location.reload()
				  });
	}
	function changeApprovalStatus(id, campaign){
	
		request = jQuery.ajax({
				type: 'post',
				  url: 'setapprovalstatus.php',
				  data: { id: id, campaign: campaign}
				  })
				    .done(function( msg ) {
					
				  });
	}

	


	function testcomment(){
	
		var text = document.getElementById("comment").value;
		
		request = jQuery.ajax({
		type: "post",
		  url: "spincomment.php",
		  data: { spintax: text }
		  })
		    .done(function( msg ) {
			
			document.getElementById("testingcomment").innerHTML = msg;
			
		});
	
				
				
	}
	function testreply(){
	
		var text = document.getElementById("reply").value;
		
		request = jQuery.ajax({
		type: "post",
		  url: "spincomment.php",
		  data: { spintax: text }
		  })
		    .done(function( msg ) {
			
			document.getElementById("testingreply").innerHTML = msg;
			
		});
	
				
				
	}
	



</script>


  <section id="main-content">

      <section class="wrapper">
		<div class="col-lg-12">
		            <!--widget start-->
                              <section class="panel">
                                  <header class="panel-heading tab-bg-dark-navy-blue">
                                      <ul class="nav nav-tabs nav-justified ">
                                          <li class="active">
                                              <a href="#setup" data-toggle="tab">
                                                  Setup
                                              </a>
                                          </li>
				    <li>
                                              <a href="#pages" data-toggle="tab" onclick = "pagesWarning();">
                                                  Pages
                                              </a>
                                          </li>	
				     <li>
                                              <a href="#viral" data-toggle="tab">
                                                  Viral 
                                              </a>
                                          </li>		
                                          <li>
                                              <a href="#feeds" data-toggle="tab">
                                                  Feeds
                                              </a>
                                          </li>
                                          <li>
                                              <a href="#video" data-toggle="tab">
                                                  Video
                                              </a>
                                          </li>
					       <? 
					if ($oto1 == "YES"){
						echo '
						<li>
						      <a href="#images" data-toggle="tab">
								Images
						      </a>
						</li>	 ';
					
					}
				
				?>	
				      <? 
					if ($oto1 == "YES"){
						echo '
						<li>
						      <a href="#custom" data-toggle="tab">
								Promotions
						      </a>
						</li>	 ';
					
					}
				
				?>	
				     <li>
                                              <a href="#comments" data-toggle="tab" onclick = "commentsWarning();">
                                                  Comments
                                              </a>
                                          </li>
			              <li>
                                              <a href="#replies" data-toggle="tab" onclick = "repliesWarning();">
                                                  Replies
                                              </a>
                                          </li>
				 <? 
					if ($oto2 == "YES"){
						echo '
						<li>
						      <a href="#list" data-toggle="tab">
								List Builder
						      </a>
						</li>	 ';
					
					}?>	
				<? 
					if ($oto1 == "YES"){
						echo '
						<li>
						      <a href="#growth" data-toggle="tab">
								Growth
						      </a>
						</li>	 ';
					
					}
				
				?>						
				<li>
                                              <a href="#log" data-toggle="tab">
                                                  Log
                                              </a>
				</li>	 
				
                                      </ul>
                                  </header>
                                  <div class="panel-body">
                                      <div class="tab-content tasi-tab">
				    
                                          <div class="tab-pane active" id="setup">
					  <script>
					  var hash = window.location.hash.substr(1);
					  if (hash == "setup"){
							request = jQuery.ajax({
							type: "get",
							  url: "setup.php"
							
							  })
							    .done(function( msg ) {								
								//document.getElementById("setup").innerHTML = msg;
								alert(msg);
						});
					}
					  </script>
                                           
                                          </div>
				<div class="tab-pane " id="pages">
                                        
                                          </div>
				  <div class="tab-pane " id="viral">
                                            
                                          </div>
                                          <div class="tab-pane" id="feeds">
					
                                          </div>
                                          <div class="tab-pane " id="video">
                                             
                                          </div>
						<? if ($oto1 == "YES"){
						echo '
						<div class="tab-pane " id="images">';
							 include 'images.php'; 
						echo ' </div>';
						}
					?>
			
					<? if ($oto1 == "YES"){
						echo '
						<div class="tab-pane " id="custom">';
							 include 'custom.php'; 
						echo ' </div>';
						}
					?>
				    <div class="tab-pane " id="comments">
                                                  
                                          </div>
				     <div class="tab-pane " id="replies">
						
                                          </div>	 
						<? if ($oto2 == "YES"){
						echo '
						<div class="tab-pane " id="list">';
							 include 'list.php'; 
						echo ' </div>';
						}
					?>	
				<? if ($oto1 == "YES"){
						echo '
						<div class="tab-pane " id="growth">';
							 include 'growth.php'; 
						echo ' </div>';
						}
					?>					
				    <div class="tab-pane " id="log">

                                          </div>	  	   
                                      </div>
                                  </div>
                              </section>
                              <!--widget end-->
		</div>


  </section>

<!--main content end-->


<?php include 'templates/footer.php'; 


?>


