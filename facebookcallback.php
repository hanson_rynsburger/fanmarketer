<?php
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

session_start();
require_once __DIR__ . '/vendor/facebook/graph-sdk/src/Facebook/autoload.php';

if (isset($_GET['userid'])) {
	$db = new PDO('mysql:host=localhost;dbname=dan_fanmarketer', 'dan_fanmarketer', 'm0nkeyBRAINS');
	$sql = "SELECT * FROM `settings` WHERE `userid` = {$_GET['userid']}";
	$stmt = $db->prepare($sql);
	$stmt->execute();

	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	$fbid = $user['fbid'];
	$secret = $user['fbsecret'];

	$userid = $_GET['userid'];

	$fb = new Facebook\Facebook([
		'app_id' => $fbid,
		'app_secret' => $secret,
		'default_graph_version' => 'v2.10',
	]);

	$helper = $fb->getRedirectLoginHelper();

	try {
		$accessToken = $helper->getAccessToken();
	} catch (Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch (Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}

	if (isset($accessToken)) {
		// Logged in!
		$_SESSION['facebook_access_token'] = (string) $accessToken;
		//	echo "access token = ";
		//	print_r($_SESSION['facebook_access_token']);

		$sql = "UPDATE settings SET fbaccesstoken='" . $_SESSION['facebook_access_token'] . "' WHERE userid=" . $userid;
		$stmt = $db->prepare($sql);
		$stmt->execute();

		echo '
		<script>
			window.location.href = "./index.php";
		</script>';
	}
}
?>
